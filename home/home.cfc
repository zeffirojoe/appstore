<cfcomponent displayname="home" hint="CFC for home">
  <cfset this.datasource="AppStore">
  <cffunction name="getList" access="remote" returnformat="plain">
    <cfquery name="productList" datasource = "#this.datasource#">
      SELECT [productID], [description], [imagePath], [price], [title] FROM Products Order By [productID] OFFSET 0 ROWS FETCH NEXT 5 ROWS ONLY;
    </cfquery>
    <cfreturn #serializeJSON(productList, "struct")#/>
  </cffunction>
</cfcomponent>
