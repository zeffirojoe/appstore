function createCarousel() {
  $('#result').hide();
  $.get("home.cfc?method=getList", function (data) {
    var items = JSON.parse(data);
    var tr = '<ol class="carousel-indicators"> <li data-target="#carousel" data-slide-to="0"></li> <li data-target="#carousel" data-slide-to="1"></li> <li data-target="#carousel" data-slide-to="2"></li><li data-target="#carousel" data-slide-to="3"></li> <li data-target="#carousel" data-slide-to="4"></li></ol>';
    tr += '<div class="carousel-inner" role="listbox">';
    for (var i = 0; i < items.length; i++) {
      tr += '<div class="carousel-item';
      if (i == 0) tr += ' active';
      tr += '"> <div class="item"><img class="center-block" src="' + items[i].imagePath + '" alt="' + items[i].title + '"> <h3>' + items[i].title + '</h3> <p>' + items[i].description + '</p> </div></div>';
    }
    tr += '<a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="carousel-control-next" href="#carousel" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a></div> ';
    $('#carousel').append(tr).show("slow");
  });
  $('#result').fadeIn();

}

$(document).ready(function () {
  $(Document).on("click", "#logout", function (e) { //logout of current user
    e.preventDefault();
    sessionStorage.removeItem('userID');
    sessionStorage.removeItem('role');
    sessionStorage.removeItem('sessionID');
    $('#logoutM').modal();
  });

});