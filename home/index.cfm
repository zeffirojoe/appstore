<html>

  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="home.js"></script>
    <link rel="stylesheet" type="text/css" href="index.css">
    <link rel="icon" type="image/ico" href="../images/image_v3_MHH_icon.ico">
    <title>Home</title>
  </head>
  <!---<cfoutput>#items#</cfoutput>--->

  <body>
    <div class="modal fade" id="logoutM" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel4" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel4">Logout</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"  onclick="location.reload()">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            Successfully Logged out of User
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="location.reload()">Close</button>
          </div>
        </div>
      </div>
    </div>
    <div class="menu">
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container" id="navbar">
          <a class="navbar-brand" href="../home">
            <img src="https://i.gyazo.com/c29162661e819a80f550c3c2897db908.png" alt="Home">
          </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item  active">
                <a class="nav-link" href="#">Home
                  <span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="../products">Products
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="../cart/cart.cfm">Cart</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="../myaccount">My Account</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
    <script>
      if(sessionStorage.getItem('role') == 3){
        $('a').filter(function(index) { return $(this).text() === "Cart";}).attr('href','../sales');
        $('a').filter(function(index) { return $(this).text() === "Cart";}).text('Sales Trends');
      }
      if(sessionStorage.getItem('role') != null){
        $('.navbar-nav').append('<li class="nav-item"><a class="nav-link" href="#" id="logout">Logout</a></li>');
      }
    </script>
    <div class="container" id="result">
      <div class="container" id="main">
        <div id="carousel" class="carousel slide" data-ride="carousel">
          <script>
            createCarousel();
          </script>
        </div>
      </div>
      <hr>
      <div class="container" id="info">
        <div class="row">
          <div class="col-md-6">
            <img class="img-fluid rounded mb-3 mb-mb 0" src="../images/rde_logo.png" id="logo">
          </div>
          <div class="col-md-6">
            <h5>RDE Systems</h5>
            <p>RDE Systems, makers of eCOMPAS, has been serving public health for over twenty-five years. We are proud that RDE has grown only by referrals, and not through marketing and sales. Our business philosophy is simple, yet refreshingly uncommon in the
              industry:
              <q>If you do good things for good people, good things happen.</q>
              We are focused on the long-term interests of our clients, and our no-commission policy allows our consults to be pressure-free.</p>
          </div>
        </div>
      </div>
    </div>
    <!-- Footer -->
    <footer class="page-footer font-small blue" id="footer">

      <!-- Copyright -->
      <div class="footer-copyright text-center py-3">
        <p id="date">
          Date
          </p>
      </div>
      <!-- Copyright -->
  
    </footer>
    <!-- Footer -->
  </body>
  <script>
    var today = new Date();
    var date = today.getFullYear();
    var dateTime = date;
    $("#date").html(dateTime + '<a href="https://www.rde.org/"> RDE Systems LLC</a>');
  </script>

</html>
