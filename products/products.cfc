<cfcomponent displayname="Products" hint="CFC for product list">
  <cfset this.datasource="AppStore">
  <cffunction name="getList" access="remote" returnformat="plain">
    <cfargument name="rows" required="false" default=0 type="numeric">
    <cfquery name="productList" datasource = "#this.datasource#">
      SELECT [productID], [description], [imagePath], [price], [title], [published] FROM Products Order By [productID] OFFSET <cfqueryparam value="#rows#" cfsqltype="cf_sql_integer"> ROWS FETCH NEXT 10 ROWS ONLY;
    </cfquery>
    <cfreturn #serializeJSON(productList, "struct")#/>
  </cffunction>

  <cffunction name="searchList" access="remote" returnformat="plain">
    <cfargument name="search" required="true">
    <cfargument name="rows" required="false" default=0 type="numeric">
    <cfquery name="productList" datasource = "#this.datasource#">
      SELECT [productID], [description], [imagePath], [price], [title], [published] FROM Products WHERE [description] LIKE <cfqueryparam value="%#search#%" > OR [title] LIKE <cfqueryparam value="%#search#%" > Order By [productID] OFFSET <cfqueryparam value="#rows#" cfsqltype="cf_sql_integer"> ROWS FETCH NEXT 10 ROWS ONLY;
    </cfquery>
    <cfreturn #serializeJSON(productList, "struct")#/>
  </cffunction>

  <cffunction name="getOrder" access="remote" returnformat="plain">
    <cfargument name="userID" required="true">
    <cfquery name="orderID" datasource="#this.datasource#">
      SELECT [orderID] FROM [Orders] WHERE userID = <cfqueryparam value="#userID#" cfsqltype="cf_sql_integer"> AND [checkedOut] IS NULL;
    </cfquery>
    <cfreturn #serializeJSON(orderID, "struct")#/>
  </cffunction>

  <cffunction name="createOrder" access="remote" returnformat="plain">
    <cfargument name="userID" required="true">
    <cfquery name="order" datasource="#this.datasource#">
      INSERT INTO [Orders] ([userID]) VALUES (#userID#);
    </cfquery>
    <cfquery name="orderID" datasource="#this.datasource#">
      SELECT [orderID] FROM [Orders] WHERE userID = <cfqueryparam value="#userID#" cfsqltype="cf_sql_integer"> AND [checkedOut] IS NULL;
    </cfquery>
    <cfreturn #serializeJSON(orderID, "struct")#/>
  </cffunction>

  <cffunction name="addItem" access="remote" returnformat="plain">
    <cfargument name="orderID" required="true">
    <cfargument name="currentItem" required="true">
    <cfset var x = urlDecode(#currentItem#)>
    <cfquery name="getProductID" datasource="#this.datasource#">
      SELECT [productID] FROM Products WHERE [title] = '#x#'      
    </cfquery>
    <cfquery name="addProduct"  datasource="#this.datasource#">
      INSERT INTO [Cart Items] ([orderID],[productID]) VALUES (<cfqueryparam value="#orderID#" cfsqltype="cf_sql_integer">,<cfqueryparam value="#getProductID.productID#" cfsqltype="cf_sql_integer">);
    </cfquery>
  </cffunction>

  <cffunction name="deleteItem" access="remote" returnformat="plain"> 
    <cfargument name="title" required="true" type="string">
    <cfset var x = urlDecode(#title#)>
    <cfquery name="deleteItem" datasource="#this.datasource#">
      DELETE FROM Products WHERE [title] = <cfqueryparam value="#x#" cfsqltype="cf_sql_varchar">;
    </cfquery>
  </cffunction>

  <cffunction name="publishItem" access="remote" returnformat="plain"> 
    <cfargument name="title" required="true" type="string">
    <cfset var x = urlDecode(#title#)>
    <cfquery name="deleteItem" datasource="#this.datasource#">
      UPDATE Products SET [published] = 1 WHERE [title] = <cfqueryparam value="#x#" cfsqltype="cf_sql_varchar">;
    </cfquery>
  </cffunction>

  <cffunction name="unpublishItem" access="remote" returnformat="plain"> 
    <cfargument name="title" required="true" type="string">
    <cfset var x = urlDecode(#title#)>
    <cfquery name="deleteItem" datasource="#this.datasource#">
      UPDATE Products SET [published] = 0 WHERE [title] = <cfqueryparam value="#x#" cfsqltype="cf_sql_varchar">;
    </cfquery>
  </cffunction>

</cfcomponent>
