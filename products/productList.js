function createList() {

  $.get("products.cfc?method=getList", function (data) {
    $('.item').each(function () {
      $(this).hide('slow', function () {
        $(this).remove();
      });
    });
    var itemList = JSON.parse(data);
    var tr;
    for (var i = 0; i < itemList.length; i++) { //populates product list with DB items from the products Table
      if (itemList[i].published == 0) continue;
      tr = '<div class="item" style="display: none;"><hr><div class="row"> <div class="col-md-6">';
      tr += '<a href="#"><img class-"img-fluid rounded mb-3 mb-mb-0" src="' + itemList[i].imagePath + '"></a>';
      tr += '</div> <div class="col-md-4">';
      tr += '<h3>' + itemList[i].title + '</h3>';
      tr += '<p>Price: $' + itemList[i].price + '</p>';
      tr += '<p>' + itemList[i].description + '</p>';
      tr += '</div> <div class="col-md-2"> <a class="btn btn-primary add" href="#">Add to Cart</a> <br><br>';
      tr += '<a class="btn btn-primary" href="#">Item Info</a> </div> </div></div>';
      $('#productList').append(tr).show("slow");
    }
    $('.item').each(function () {
      $(this).fadeIn();
    });

  });
}

function delay(callback, ms) { //created .5 second delay which is ued for keyup filtering
  var timer = 0;
  return function () {
    var context = this,
      args = arguments;
    clearTimeout(timer);
    timer = setTimeout(function () {
      callback.apply(context, args);
    }, ms || 0);
  };
}

function addItem() {
  $.get('products.cfc?method=getOrder&userID=' + sessionStorage.getItem('userID'), function (data) {
    if (data == '[]') {
      $.get('products.cfc?method=createOrder&userID=' + sessionStorage.getItem('userID'), function (data) {
        var order = JSON.parse(data);
        sessionStorage.setItem('orderID', order[0].orderID);
      });
    } else {
      var order = JSON.parse(data);
      sessionStorage.setItem('orderID', order[0].orderID);
    }
    $.get('products.cfc?method=addItem&orderID=' + sessionStorage.getItem('orderID') + '&currentItem=' + encodeURI(sessionStorage.getItem('currentItem')), function (data) {
      //alert('Added Successfully!');
    });
  });
  $('#myModal').modal();
}


$(document).ready(function () {

  $(Document).on("click", ".item a", function () { //handler for all links for each item, grabs item title
    if ($(this).text().toLowerCase() == 'add to cart') {
      if (sessionStorage.getItem('role') == null) window.location.replace("../myaccount"); //redirects if user isnt logged in
      else {
        sessionStorage.setItem('currentItem', $(this).closest(".item").find("h3").text());
        addItem();
      }
    } else if ($(this).text() == 'Item Info' || $(this).text() == null) {
      sessionStorage.setItem('currentItem', $(this).closest(".item").find("h3").text());
      window.location.replace('../item');
    } else {
      sessionStorage.setItem('currentItem', $(this).closest(".item").find("h3").text());
      window.location.replace('../item');
    }
  });

  $(Document).on("click", "#logout", function (e) { //logout of current user
    e.preventDefault();
    sessionStorage.removeItem('userID');
    sessionStorage.removeItem('role');
    sessionStorage.removeItem('sessionID');
    $('#logoutM').modal();
  });

  $('.pagination a').click(function () { //functionality for pagination, repopulates page with items 0-10,10-20, etc...
    var offset = parseInt($(this).text());
    offset = (offset * 10) - 10;
    $('.item').each(function () {
      $(this).hide('slow', function () {
        $(this).remove();
      });
    });
    var text = $('#filter').val().toLowerCase();
    $.get("products.cfc?method=searchList&search=" + text + "&rows=" + offset, function (data) {
      var itemList = JSON.parse(data);
      var tr;
      for (var i = 0; i < itemList.length; i++) { //populates product list with DB items from the products Table
        if (itemList[i].published == 0) continue;
        tr = '<div class="item" style="display: none;"><hr><div class="row"> <div class="col-md-6">';
        tr += '<a href="#"><img class-"img-fluid rounded mb-3 mb-mb-0" src="' + itemList[i].imagePath + '"></a>';
        tr += '</div> <div class="col-md-4">';
        tr += '<h3>' + itemList[i].title + '</h3>';
        tr += '<p>Price: $' + itemList[i].price + '</p>';
        tr += '<p>' + itemList[i].description + '</p>';
        tr += '</div> <div class="col-md-2"> <a class="btn btn-primary add" href="#">Add to Cart</a> <br><br>';
        tr += '<a class="btn btn-primary" href="#">Item Info</a> </div> </div></div>';
        $('#productList').append(tr).show("slow");
      }
      $('.item').each(function () {
        $(this).fadeIn();
      });
    });
  });

  $('#filter').keyup(delay(function (e) { //keyup handler for filtering, queries DB for new info .5 seconds after user stops typing
    var text = $(this).val().toLowerCase();
    if (text == "" || text == " ") {
      createList();
      return;
    }
    $('.item').each(function () {
      $(this).hide('slow', function () {
        $(this).remove();
      });
    });
    $.get("products.cfc?method=searchList&search=" + text, function (data) {
      var itemList = JSON.parse(data);
      var tr;
      for (var i = 0; i < itemList.length; i++) { //populates product list with DB items from the products Table
        if (itemList[i].published == 0) continue;
        tr = '<div class="item" style="display: none;"><hr><div class="row"> <div class="col-md-6">';
        tr += '<a href="#"><img class-"img-fluid rounded mb-3 mb-mb-0" src="' + itemList[i].imagePath + '"></a>';
        tr += '</div> <div class="col-md-4">';
        tr += '<h3>' + itemList[i].title + '</h3>';
        tr += '<p>Price: $' + itemList[i].price + '</p>';
        tr += '<p>' + itemList[i].description + '</p>';
        tr += '</div> <div class="col-md-2"> <a class="btn btn-primary add" href="#">Add to Cart</a> <br><br>';
        tr += '<a class="btn btn-primary" href="#">Item Info</a> </div> </div></div>';
        $('#productList').append(tr).show("slow");
      }
      $('.item').each(function () {
        $(this).fadeIn();
      });
    });
  }, 500));
  $('.dropdown-menu a').click(function () { //Filtering for dropdown
    var text = $(this).text().toLowerCase();
    if (text == "reset items") {
      createList();
      return;
    }
    $('.item').each(function () {
      $(this).hide('slow', function () {
        $(this).remove();
      });
    });
    $.get("products.cfc?method=searchList&search=" + text, function (data) {
      var itemList = JSON.parse(data);
      var tr;
      for (var i = 0; i < itemList.length; i++) { //populates product list with DB items from the products Table
        if (itemList[i].published == 0) continue;
        tr = '<div class="item"><hr><div class="row"> <div class="col-md-6">';
        tr += '<a href="#"><img class-"img-fluid rounded mb-3 mb-mb-0" src="' + itemList[i].imagePath + '"></a>';
        tr += '</div> <div class="col-md-4">';
        tr += '<h3>' + itemList[i].title + '</h3>';
        tr += '<p>' + itemList[i].description + '</p>';
        tr += '<p>Price: $' + itemList[i].price + '</p>';
        tr += '</div> <div class="col-md-2"> <a class="btn btn-primary add" href="#">Add to Cart</a> <br><br>';
        tr += '<a class="btn btn-primary" href="#">Item Info</a> </div> </div></div>';
        $('#productList').append(tr).show("slow");
      }
    });
  });
});