function createList() {
  if(!$('#create').length) $('#row1').append('<div id="create"> <a class="btn btn-primary" id="createBTN" href="#">Create Item</a> </div> ');
  $.get("products.cfc?method=getList", function (data) {
    $('.item').each(function () {
      $(this).hide('slow', function () {
        $(this).remove();
      });
    });
    var itemList = JSON.parse(data);
    var tr;
    for (var i = 0; i < itemList.length; i++) { //populates product list with DB items from the products Table
      tr = '<div class="item" style="display: none;"><hr><div class="row"> <div class="col-md-6">';
      tr += '<a href="#"><img class-"img-fluid rounded mb-3 mb-mb-0" src="' + itemList[i].imagePath + '"></a>';
      tr += '</div> <div class="col-md-4">';
      tr += '<h3>' + itemList[i].title + '</h3>';
      tr += '<p>Price: $' + itemList[i].price + '</p>';
      tr += '<p>' + itemList[i].description + '</p>';
      if(itemList[i].published == 1) tr += '<h4 style="color: green; font-weight: bold;">PUBLISHED</h4>';
      else tr += '<h4 style="color: red; font-weight: bold;">UNPUBLISHED</h4>';
      tr += '</div> <div class="col-md-2">';
      tr += '<div class="dropdown" id="adminTools"><button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" >Admin Tools</button><div class="dropdown-menu"><a class="dropdown-item" href="#">Edit</a><a class="dropdown-item" href="#">Publish/Unpublish</a><a class="dropdown-item" href="#">Delete</a></div></div>';
      //tr += '<a class="btn btn-primary add" href="#">Add to Cart</a> <br><br>';
      tr += '<a class="btn btn-primary" href="#">Item Info</a> </div> </div></div>';
      $('#productList').append(tr).show("slow");
    }
    $('.item').each(function () {
      $(this).fadeIn();
    });

  });
}

function delay(callback, ms) { //created .5 second delay which is ued for keyup filtering
  var timer = 0;
  return function () {
    var context = this,
      args = arguments;
    clearTimeout(timer);
    timer = setTimeout(function () {
      callback.apply(context, args);
    }, ms || 0);
  };
}

function addItem() {
  $.get('products.cfc?method=getOrder&userID=' + sessionStorage.getItem('userID'), function (data) {
    if (data == '[]') {
      $.get('products.cfc?method=createOrder&userID=' + sessionStorage.getItem('userID'), function (data) {
        var order = JSON.parse(data);
        sessionStorage.setItem('orderID', order[0].orderID);
      });
    } else {
      var order = JSON.parse(data);
      sessionStorage.setItem('orderID', order[0].orderID);
    }
    $.get('products.cfc?method=addItem&orderID=' + sessionStorage.getItem('orderID') + '&currentItem=' + encodeURI(sessionStorage.getItem('currentItem')), function (data) {
      //alert('Added Successfully!');
      $('#myModal').modal();
    });
  });
}

function publishing(state){
  if(state == 'PUBLISHED')$('#unpublish').modal(); 
  else $('#publish').modal();  
}

$(document).ready(function () {

  $(Document).on("click", ".item a", function () { //handler for all links for each item, grabs item title
    if ($(this).text().toLowerCase() == 'add to cart') {
      if (sessionStorage.getItem('role') == null) window.location.replace("../myaccount"); //redirects if user isnt logged in
      else {
        sessionStorage.setItem('currentItem', $(this).closest(".item").find("h3").text());
        addItem();
      }
    } else if ($(this).text() == 'Publish/Unpublish') {
      sessionStorage.setItem('currentItem', $(this).closest(".item").find("h3").text());
      publishing($(this).closest(".item").find("h4").text());
    } else if ($(this).text() == 'Item Info') {
      sessionStorage.setItem('currentItem', $(this).closest(".item").find("h3").text());
      window.location.replace('../item');
    } else if ($(this).text() == 'Delete') {
      sessionStorage.setItem('currentItem', $(this).closest(".item").find("h3").text());
      $('#confirm').modal();
    } else if ($(this).text() == 'Edit'){
      sessionStorage.setItem('currentItem', $(this).closest(".item").find("h3").text());
      window.location.replace('../item/editProduct.cfm');
    }
    else{
      sessionStorage.setItem('currentItem', $(this).closest(".item").find("h3").text());
      window.location.replace('../item');
    }
  });
  $('#createBTN').click(function () { //create item button handler
    window.location.href = '../item/createProduct.cfm';
  });

  $(Document).on("click", "#delete", function () { //handles deletion of items after modal confirmation
    $.get('products.cfc?method=deleteItem&title=' + encodeURI(sessionStorage.getItem('currentItem')), function (data) {
      location.reload();
    });
  });

  $(Document).on("click", "#publishBTN", function () { //handles publishing of items after modal confirmation
    $.get('products.cfc?method=publishItem&title=' + encodeURI(sessionStorage.getItem('currentItem')), function (data) {
      location.reload();
    });
  });

  $(Document).on("click", "#unpublishBTN", function () { //handles unpublishing of items after modal confirmation
    $.get('products.cfc?method=unpublishItem&title=' + encodeURI(sessionStorage.getItem('currentItem')), function (data) {
      location.reload();
    });
  });

  $(Document).on("click", "#logout", function (e) { //logout of current user
    e.preventDefault();
    sessionStorage.removeItem('userID');
    sessionStorage.removeItem('role');
    sessionStorage.removeItem('sessionID');
    $('#logoutM').modal();
  });
  
  $('.pagination a').click(function () { //functionality for pagination, repopulates page with items 0-10,10-20, etc...
    var offset = parseInt($(this).text());
    offset = (offset * 10) - 10;
    $('.item').each(function () {
      $(this).hide('slow', function () {
        $(this).remove();
      });
    });
    var text = $('#filter').val().toLowerCase();
    $.get("products.cfc?method=searchList&search=" + text + "&rows=" + offset, function (data) {
      var itemList = JSON.parse(data);
      var tr;
      for (var i = 0; i < itemList.length; i++) { //populates product list with DB items from the products Table
        tr = '<div class="item" style="display: none;"><hr><div class="row"> <div class="col-md-6">';
        tr += '<a href="#"><img class-"img-fluid rounded mb-3 mb-mb-0" src="' + itemList[i].imagePath + '"></a>';
        tr += '</div> <div class="col-md-4">';
        tr += '<h3>' + itemList[i].title + '</h3>';
        tr += '<p>Price: $' + itemList[i].price + '</p>';
        tr += '<p>' + itemList[i].description + '</p>';
        if(itemList[i].published == 1) tr += '<h4 style="color: green; font-weight: bold;">PUBLISHED</h4>';
        else tr += '<h4 style="color: red; font-weight: bold;">UNPUBLISHED</h4>';
        tr += '</div> <div class="col-md-2">';
        tr += '<div class="dropdown"  id="adminTools"><button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" >Admin Tools</button><div class="dropdown-menu"><a class="dropdown-item" href="#">Edit</a><a class="dropdown-item" href="#">Publish/Unpublish</a><a class="dropdown-item" href="#">Delete</a></div></div>';
        //tr += '<a class="btn btn-primary add" href="#">Add to Cart</a> <br><br>';
        tr += '<a class="btn btn-primary" href="#">Item Info</a> </div> </div></div>';
        $('#productList').append(tr).show("slow");
      }
      $('.item').each(function () {
        $(this).fadeIn();
      });
    });
  });

  $('#filter').keyup(delay(function (e) { //keyup handler for filtering, queries DB for new info .5 seconds after user stops typing
    var text = $(this).val().toLowerCase();
    if (text == "" || text == " ") {
      createList();
      return;
    }
    $('.item').each(function () {
      $(this).hide('slow', function () {
        $(this).remove();
      });
    });
    $.get("products.cfc?method=searchList&search=" + text, function (data) {
      var itemList = JSON.parse(data);
      var tr;
      for (var i = 0; i < itemList.length; i++) { //populates product list with DB items from the products Table
        tr = '<div class="item" style="display: none;"><hr><div class="row"> <div class="col-md-6">';
        tr += '<a href="#"><img class-"img-fluid rounded mb-3 mb-mb-0" src="' + itemList[i].imagePath + '"></a>';
        tr += '</div> <div class="col-md-4">';
        tr += '<h3>' + itemList[i].title + '</h3>';
        tr += '<p>Price: $' + itemList[i].price + '</p>';
        tr += '<p>' + itemList[i].description + '</p>';
        if(itemList[i].published == 1) tr += '<h4 style="color: green; font-weight: bold;">PUBLISHED</h4>';
        else tr += '<h4 style="color: red; font-weight: bold;">UNPUBLISHED</h4>';
        tr += '</div> <div class="col-md-2">';
        tr += '<div class="dropdown" id="adminTools"><button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" >Admin Tools</button><div class="dropdown-menu"><a class="dropdown-item" href="#">Edit</a><a class="dropdown-item" href="#">Publish/Unpublish</a><a class="dropdown-item" href="#">Delete</a></div></div>';
        //tr += '<a class="btn btn-primary add" href="#">Add to Cart</a> <br><br>';
        tr += '<a class="btn btn-primary" href="#">Item Info</a> </div> </div></div>';
        $('#productList').append(tr).show("slow");
      }
      $('.item').each(function () {
        $(this).fadeIn();
      });
    });
  }, 500));
  $('.dropdown-menu a').click(function () { //Filtering for dropdown
    var text = $(this).text().toLowerCase();
    if (text == "reset items") {
      createList();
      return;
    }
    $('.item').each(function () {
      $(this).hide('slow', function () {
        $(this).remove();
      });
    });
    $.get("products.cfc?method=searchList&search=" + text, function (data) {
      var itemList = JSON.parse(data);
      var tr;
      for (var i = 0; i < itemList.length; i++) { //populates product list with DB items from the products Table
        tr = '<div class="item" style="display: none;"><hr><div class="row"> <div class="col-md-6">';
        tr += '<a href="#"><img class-"img-fluid rounded mb-3 mb-mb-0" src="' + itemList[i].imagePath + '"></a>';
        tr += '</div> <div class="col-md-4">';
        tr += '<h3>' + itemList[i].title + '</h3>';
        tr += '<p>Price: $' + itemList[i].price + '</p>';
        tr += '<p>' + itemList[i].description + '</p>';
        if(itemList[i].published == 1) tr += '<h4 style="color: green; font-weight: bold;">PUBLISHED</h4>';
        else tr += '<h4 style="color: red; font-weight: bold;">UNPUBLISHED</h4>';
        tr += '</div> <div class="col-md-2">';
        tr += '<div class="dropdown"><button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" >Admin Tools</button><div class="dropdown-menu"><a class="dropdown-item" href="#">Edit</a><a class="dropdown-item" href="#">Publish/Unpublish</a><a class="dropdown-item" href="#">Delete</a></div></div>';
        //tr += '<a class="btn btn-primary add" href="#">Add to Cart</a> <br><br>';
        tr += '<a class="btn btn-primary" href="#">Item Info</a> </div> </div></div>';
        $('#productList').append(tr).show("slow");
      }
    });
  });
});