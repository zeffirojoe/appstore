function populatePage() {
  $.get('item.cfc?method=info&title=' + encodeURI(sessionStorage.getItem('currentItem')), function (data) {
    var item = JSON.parse(data);
    var tr = '<main class="bg-light"> <div class="container bg-white" id="createProduct"> <h1 style="text-align:left;padding:60px 0px 0px 0px">' + item[0].title;
    if(sessionStorage.getItem('role') != 3)tr += '<button type="button" id="add" style="float:right" class="btn btn-success">Add to Cart</button>';
    tr += '</h1> <!--float:left;--><hr> <form id="form" autocomplete="off"> <aside> <div class="uploadImageMain"> <img id="main" src="' + item[0].imagePath + '" style="object-fit:contain;width:360px;height:360px;" alt="' + item[0].title + '" /> </div> </aside> <section> <div class="prodNamePriceTags"> <h5 class="col" style="float:right;width:100%">' + item[0].title + '</h5> <br> <h6 class="col" style="float:right;width:100%;font-weight:normal">$' + item[0].price + '</h6> <br> <br> <div class="row" style="height:290px;width:100%;float:right"> <br> ' + item[0].description + ' </div><br> </div> </section> <br> <div class="col center2"> <center><strong>Tech Specs</strong></center><hr> <div class="col" style="width:100%;float:right">'; 
    if(item[0].techspecs == null || item[0].techspecs == '' || item[0].techspecs.toLowerCase() == 'null') tr += 'Here are some Specs. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
    else tr += item[0].techspecs;
    tr += '</div><br><br><br> </div> <hr> <br><br><br><hr> </form> </div> </main>';
    $('#main').hide();
    $('#main').append(tr);
    $('#main').fadeIn();
  });
}


function addItem() {
  $.get('item.cfc?method=getOrder&userID=' + sessionStorage.getItem('userID'), function (data) {
    if (data == '[]') {
      $.get('item.cfc?method=createOrder&userID=' + sessionStorage.getItem('userID'), function (data) {
        var order = JSON.parse(data);
        sessionStorage.setItem('orderID', order[0].orderID);
      });
    } else {
      var order = JSON.parse(data);
      sessionStorage.setItem('orderID', order[0].orderID);
    }
    $.get('item.cfc?method=addItem&orderID=' + sessionStorage.getItem('orderID') + '&currentItem=' + encodeURI(sessionStorage.getItem('currentItem')), function (data) {
      //alert('Added Successfully!');
    });
  });
  $('#myModal').modal();
}
$(document).ready(function () {
  $(Document).on("click", "#add", function () {
    if ($(this).text().toLowerCase() == 'add to cart') {
      if (sessionStorage.getItem('role') == null) window.location.replace("../myaccount"); //redirects if user isnt logged in
      else {
        addItem();
      }
    }
  });

  $(Document).on("click", "#logout", function (e) { //logout of current user
    e.preventDefault();
    sessionStorage.removeItem('userID');
    sessionStorage.removeItem('role');
    sessionStorage.removeItem('sessionID');
    $('#logoutM').modal();
  });

});