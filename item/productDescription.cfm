<html>
<head>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="productList.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="productDescription.css">
	<title>Product Description</title>
</head>
	<!---<cfoutput>#items#</cfoutput>--->

<body>
  <div class="menu">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark navbar-static-top">
      <div class="container" id="navbar">
        <a class="navbar-brand" href="#">
          <img src="https://i.gyazo.com/c29162661e819a80f550c3c2897db908.png" alt="Home">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
          aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="../home">Home </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../products">Products
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../cart/cart.cfm">Cart</a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="#">My Account</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </div>
<main class="bg-light">
	<div class="container bg-white" id="createProduct">
		<h1 style="text-align:left;padding:60px 0px 0px 0px">Product Name
		<button type="button" id="Save" style="float:right" class="btn btn-success" onclick="window.location.href='createProduct.html'">Add to Cart</button></h1>
		<!--float:left;--><hr>
		<form id="form" autocomplete="off">
			<aside>
				<div class="uploadImageMain">
					<img id="main" src="images\placeholders\uploadImage.jpg" style="object-fit:contain;width:360px;height:360px;border-radius:5%;" alt="your image" />
				</div>
			</aside>
			<section>
				<div class="prodNamePriceTags">
					<h5 class="col" style="float:right;width:100%">Product Name</h5>
					<br>
					<h6 class="col" style="float:right;width:100%;font-weight:normal">$0.00</h6>
					<br>
					<!--
					<div class="col" style="width:100%;float:right">
						Rating: 
						<span class=" fa fa-star checked"></span>
						<span class=" fa fa-star checked"></span>
						<span class=" fa fa-star checked"></span>
						<span class=" fa fa-star"></span>
						<span class=" fa fa-star"></span>
					</div>
					-->
					<!--
					<input type="text" class="form-control" style="float:right;width:100%;border-style:hidden" align="right" name="productName" id="productName" value="Product Name" readonly><br><br>
					-->
					<!--
					<input type="text" class="form-control" style="float:right;width:100%;border-style:hidden" align="right" name="price" id="price" value="$0.00" readonly><br><br>
					Description: <br>
					-->
					<br>
					<div class="row" style="height:290px;width:100%;float:right">
						<br>
						Here is a Description. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do 
						eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis 
						aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla 
						pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia 
						deserunt mollit anim id est laborum.
					</div><br> <!--  <input type="text" name="tags" id="tags" style="height:235px;width:100%"> -->
				</div>
			</section>
			<br>
			<div class="col center2">
				<center><strong>Tech Specs</strong></center><hr>
				<div class="col" style="width:100%;float:right">
					Here are some Specs. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do 
					eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis 
					aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla 
					pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia 
					deserunt mollit anim id est laborum.
				</div><br><br><br>
			</div>
			<hr>
			<!--
			<div class="col">
				<center><strong>Customer Reviews</strong></center><hr>
				<div class="col" style="width:100%;float:left">
					<img id="profile" src="images\misc\blank-profile.png" style="object-fit:contain;width:35px;height:35px;border-radius:50%;" alt="your image" />
					<strong>Guest</Strong><br>
					Rating: 
					<span class=" fa fa-star checked"></span>
					<span class=" fa fa-star checked"></span>
					<span class=" fa fa-star checked"></span>
					<span class=" fa fa-star"></span>
					<span class=" fa fa-star"></span><br>
					This is one review. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do 
					eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis 
					aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla 
					pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia 
					deserunt mollit anim id est laborum.
				</div><br><br><br>
			</div>
			-->
			<br><br><br><hr>
		</form>
	</div>
</main>

<footer class="center3">
	<text style="font-size:10;float:right">Copyright &copy 2019 RDE Systems</text>
</footer>

<!--
<div id="footer">
  <footer class="page-footer font-small blue">
    <!-- Copyright --
    <div class="footer-copyright text-center py-3">
      <p id="date"> Date </p>
      <a href="https://www.rde.org/"> RDE Systems LLC</a>
    </div>
    <!-- Copyright --
  </footer>
</div>
-->
<!--<div class="container" >
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
</div>-->


</html>
