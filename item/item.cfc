<cfcomponent displayname="Products" hint="CFC for product list">
    <cfset this.datasource="AppStore">
    <cffunction name="info" access="remote" returnformat="plain">
        <cfargument name="title" required="true" type="string">
        <cfset var x = urlDecode(#title#)>
        <cfquery name="description" datasource="#this.datasource#">
            SELECT [title],[price],[description],[imagePath],[techSpecs],[tags],[productID] FROM Products WHERE [title] = <cfqueryparam value="#x#" cfsqltype="cf_sql_varchar">      
        </cfquery>
        <cfreturn #serializeJSON(description,"struct")#/>
    </cffunction>

    <cffunction name="getOrder" access="remote" returnformat="plain">
      <cfargument name="userID" required="true">
      <cfquery name="orderID" datasource="#this.datasource#">
        SELECT [orderID] FROM [Orders] WHERE userID = <cfqueryparam value="#userID#" cfsqltype="cf_sql_integer"> AND [checkedOut] IS NULL;
      </cfquery>
      <cfreturn #serializeJSON(orderID, "struct")#/>
    </cffunction>
  
    <cffunction name="createOrder" access="remote" returnformat="plain">
      <cfargument name="userID" required="true">
      <cfquery name="order" datasource="#this.datasource#">
        INSERT INTO [Orders] ([userID]) VALUES (#userID#);
      </cfquery>
      <cfquery name="orderID" datasource="#this.datasource#">
        SELECT [orderID] FROM [Orders] WHERE userID = <cfqueryparam value="#userID#" cfsqltype="cf_sql_integer"> AND [checkedOut] IS NULL;
      </cfquery>
      <cfreturn #serializeJSON(orderID, "struct")#/>
    </cffunction>
  
    <cffunction name="addItem" access="remote" returnformat="plain">
      <cfargument name="orderID" required="true">
      <cfargument name="currentItem" required="true">
      <cfset var x = urlDecode(#currentItem#)>
      <cfquery name="getProductID" datasource="#this.datasource#">
        SELECT [productID] FROM Products WHERE [title] = <cfqueryparam value="#x#" cfsqltype="cf_sql_varchar">     
      </cfquery>
      <cfquery name="addProduct"  datasource="#this.datasource#">
        INSERT INTO [Cart Items] ([orderID],[productID]) VALUES (<cfqueryparam value="#orderID#" cfsqltype="cf_sql_integer">,#getProductID.productID#);
      </cfquery>
    </cffunction>

    <cffunction  name="createItem" access="remote" returnformat="plain">
      <cfargument  name="productName" required="true">
      <cfargument  name="price" required="true">
      <cfargument  name="desc" required="true">
      <cfargument  name="specs" required="true">
      <cfargument  name="tags" required="true" default="">
      <cfquery name="save" datasource="#this.datasource#">
        INSERT INTO [Products] ([title],[price],[description],[tags],[techSpecs]) VALUES (<cfqueryparam value="#productName#" cfsqltype="cf_sql_varchar">,<cfqueryparam value="#price#" cfsqltype="cf_sql_float">,<cfqueryparam value="#desc#" cfsqltype="cf_sql_varchar">,<cfqueryparam value="#tags#" cfsqltype="cf_sql_varchar">,<cfqueryparam value="#specs#" cfsqltype="cf_sql_varchar">);
      </cfquery>
    </cffunction>

    <cffunction  name="editItem" access="remote" returnformat="plain">
      <cfargument  name="productName" required="true">
      <cfargument  name="price" required="true">
      <cfargument  name="desc" required="true">
      <cfargument  name="specs" required="true">
      <cfargument  name="tags" required="true" default="">
      <cfargument  name="productID" required="true">
      <cfquery name="save" datasource="#this.datasource#">
        UPDATE [Products] SET [title]=<cfqueryparam value="#productName#" cfsqltype="cf_sql_varchar">,[price]=<cfqueryparam value="#price#" cfsqltype="cf_sql_float">,[techSpecs]=<cfqueryparam value="#specs#" cfsqltype="cf_sql_varchar">,[description]=<cfqueryparam value="#desc#" cfsqltype="cf_sql_varchar">,[tags]=<cfqueryparam value="#tags#" cfsqltype="cf_sql_varchar"> WHERE [productID] = #productID#;
      </cfquery>
    </cffunction>

  </cfcomponent>
  