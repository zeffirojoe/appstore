<html>
<head>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="productList.js"></script>
	<script src="createEdit.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="createProduct.css">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
  </script>
	<title>Create Product</title>
</head>
	<!---<cfoutput>#items#</cfoutput>--->

<body>
	<script>
    if (sessionStorage.getItem('role') != 3) window.location.replace('../products');
  </script>
	<div class="modal fade" id="logoutM" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel4" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel4">Logout</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="location.reload()">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					Successfully Logged out of User
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal"  onclick="location='../home/index.cfm'">Close</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Item Saved</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Item Successfully Saved
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="location='../products/index.cfm'">Close</button>
        </div>
      </div>
    </div>
  </div>
  <div class="menu">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark navbar-static-top">
      <div class="container" id="navbar">
        <a class="navbar-brand" href="../home">
          <img src="https://i.gyazo.com/c29162661e819a80f550c3c2897db908.png" alt="Home">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
          aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="../home">Home </a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="../products">Products
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../cart/cart.cfm">Cart</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../myaccount">My Account</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
	</div>
	<script>
    if(sessionStorage.getItem('role') == 3){
      $('a').filter(function(index) { return $(this).text() === "Cart";}).attr('href','../sales');
      $('a').filter(function(index) { return $(this).text() === "Cart";}).text('Sales Trends');
    }
    if(sessionStorage.getItem('role') != null){
      $('.navbar-nav').append('<li class="nav-item"><a class="nav-link" href="#" id="logout">Logout</a></li>');
    }
  </script>
<main class="bg-light" style="padding-bottom:40px">
	<div class="container bg-white" id="createProduct">
		<h1 style="text-align:left;padding:60px 0px 0px 0px">Create Product
		<button type="button" id="preview" style="float:right" class="btn btn-dark" onclick="showPreview()">Customer Preview</button>
		<button type="button" id="exitPreview" style="float:right;display:none" class="btn btn-dark" onclick="showEdit()">Exit Preview</button>
		</h1>
		<!--float:left;--><hr>
		<form id="form" autocomplete="off">
			<aside>
				<div class="image-upload">
					<label for="mainImage">
						<img id="main" src="images\placeholders\uploadImage.jpg" style="object-fit:contain;width:360px;height:360px;border-radius:5%;" alt="your image" />
					</label>
					<input type="file" id="mainImage" style="display:none" onchange="readURL(this)"/>
				</div>
			</aside>
			<section>
				<div id="editNamePriceDesc">
					<input type="text" class="form-control" style="float:right;width:100%" align="right" name="productName" id="productName" placeholder="Product Name"><br><br>
					<input type="number" min="0.01" step="0.01" onchange="setTwoNumberDecimal(this)" class="form-control" style="float:right;width:100%" align="right" name="price" id="price" placeholder="$0.00"><br><br>
					Description: <br>
					<textarea id="desc" name="desc" class="form-control" style="height:240px;width:100%" ></textarea><br> <!--  <input type="text" name="tags" id="tags" style="height:235px;width:100%"> -->
				</div>
				<div id="previewNameDesc" style="display:none">
					<h5 class="col" id="namePreview" style="float:right;width:100%">Product Name</h5>
					<br>
					<h6 class="col" id="pricePreview" style="float:right;width:100%;font-weight:normal">$10.00</h6>
					<br>
					<!--
					<div class="col" style="width:100%;float:right">
						Rating: 
						<span class=" fa fa-star previewStar"></span>
						<span class=" fa fa-star previewStar"></span>
						<span class=" fa fa-star previewStar"></span>
						<span class=" fa fa-star previewStar"></span>
						<span class=" fa fa-star previewStar"></span>
					</div>
					-->
					<!--
					<input type="text" class="form-control" style="float:right;width:100%;border-style:hidden" align="right" name="productName" id="productName" value="Product Name" readonly><br><br>
					-->
					<!--
					<input type="text" class="form-control" style="float:right;width:100%;border-style:hidden" align="right" name="price" id="price" value="$0.00" readonly><br><br>
					Description: <br>
					-->
					<br>
					<div class="row" id="descPreview" style="height:300px;width:100%;float:right">
						<br>
						Here is a Description. It describes what the product is and its useful features.
					</div><br> <!--  <input type="text" name="tags" id="tags" style="height:235px;width:100%"> -->
				</div>
			</section>
			<br>
			<div class="center2" id="editAdvOpt">
				<center><strong>Tech Specs</strong></center><hr>
				<textarea id="specs" name="specs" class="form-control" style="height:240px;width:100%" ></textarea><br>
				<center><strong>Advanced Options</strong></center><hr><br>
				Tags: <input type="text" id="tags" name="tags" class="form-control" style="float:right;width:95%" ><br><br>
				<!--<input type="text" name="tags" id="tags" style="height:200px;width:100%"><br>-->
				<!--Supplementary Images: 
				<a href="createProduct.html">
					<img id="sup1" src="images\placeholders\uploadImage.jpg" style="object-fit:contain;width:75px;height:75px;border-radius:5%;" alt="your image" /> 
				</a>
				<a href="createProduct.html">
					<img id="sup2" src="images\placeholders\uploadImage.jpg" style="object-fit:contain;width:75px;height:75px;border-radius:5%;" alt="your image" />
				</a>
				<a href="createProduct.html">
					<img id="sup3" src="images\placeholders\uploadImage.jpg" style="object-fit:contain;width:75px;height:75px;border-radius:5%;" alt="your image" />
				</a>
				-->
			</div>
			<div class="col center2" id="previewTechSpec" style="display:none">
				<center><strong>Tech Specs</strong></center><hr>
				<div class="col" id="specsPreview" style="width:100%;float:right">
					Not Applicable.
				</div><br><br><br>
			</div>
			<hr>
			<div class="center" id="saveCancelButtons">
				<!--<input type="submit" style="float:left" value="Save Changes">-->
				
				<button type="button" id="Save" style="float:left" class="btn btn-primary" onclick="saveData()">Save Changes</button>
				
				<!--<input type="submit" style="float:right" value="Cancel">-->
				<a href="../products/index.cfm">
					<button type="button" id="Cancel" style="float:right" class="btn btn-secondary">Cancel</button>
				</a>
			</div>
			<!--
			<div class="col" id="custReviews" style="display:none">
				<center><strong>Customer Reviews</strong></center><hr>
				<div class="col" style="width:100%;float:left">
					<img id="profile" src="images\misc\blank-profile.png" style="object-fit:contain;width:35px;height:35px;border-radius:50%;" alt="your image" />
					<strong>Guest</Strong><br>
					Rating: 
					<span class=" fa fa-star previewStar"></span>
					<span class=" fa fa-star previewStar"></span>
					<span class=" fa fa-star previewStar"></span>
					<span class=" fa fa-star previewStar"></span>
					<span class=" fa fa-star previewStar"></span><br>
					This is one review. It helps customers know if the product is good and gives feedback to the creators.
				</div><br><br><br>
			</div>
			-->
			<br><br>
		</form>
	</div>
</main>

<footer class="page-footer font-small blue" id="footer">

	<!-- Copyright -->
	<div class="footer-copyright text-center py-3">
		<p id="date">
			Date
			</p>
	</div>
	<!-- Copyright -->

</footer>
<!-- Footer -->
</body>
<script>
	var today = new Date();
	var date = today.getFullYear();
	var dateTime = date;
	$("#date").html(dateTime + '<a href="https://www.rde.org/"> RDE Systems LLC</a>');
</script>

<!--
<footer class="center3">
	<text style="font-size:10;float:right">Copyright &copy 2019 RDE Systems</text>
</footer>
-->
<!--
<div id="footer">
  <footer class="page-footer font-small blue">
    <!-- Copyright --
    <div class="footer-copyright text-center py-3">
      <p id="date"> Date </p>
      <a href="https://www.rde.org/"> RDE Systems LLC</a>
    </div>
    <!-- Copyright --
  </footer>
</div>
-->
<!--<div class="container" >
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
</div>-->

</html>
