var productID;

function saveData() {
    var name = $('#productName').val();
    var price = $('#price').val();
    price = parseFloat(price);
    var desc = $('#desc').val();
    var specs = $('#specs').val();
    var tags = $('#tags').val();
    //debugger;
    $.get('item.cfc?method=createItem&productName=' + name + '&price=' + price + '&desc=' + desc + '&specs=' + specs + '&tags=' + tags);
    //var modal = $('#myModal');
    $('#myModal').modal();
    //debugger;
}

function editData() {
    var name = $('#productName').val();
    var price = $('#price').val();
    price = parseFloat(price);
    var desc = $('#desc').val();
    var specs = $('#specs').val();
    var tags = $('#tags').val();
    //debugger;
    $.get('item.cfc?method=editItem&productName=' + name + '&price=' + price + '&desc=' + desc + '&specs=' + specs + '&tags=' + tags + '&productID=' + productID);
    //var modal = $('#myModal');
    $('#myModal').modal();
    //debugger;
}

function populatePage() {
    $.get('item.cfc?method=info&title=' + encodeURI(sessionStorage.getItem('currentItem')), function (data) {
        var item = JSON.parse(data);
        productID = item[0].productID;
        debugger;
        var tr = '<main class="bg-light"> <div class="container bg-white" id="createProduct"> <h1 style="text-align:left;padding:60px 0px 0px 0px">Edit Product';
        if(sessionStorage.getItem('role') != 3)tr += '<button type="button" id="add" style="float:right" class="btn btn-success">Add to Cart</button>';
        //tr += '</h1> <!--float:left;--><hr> <form id="form" autocomplete="off"> <aside> <div class="uploadImageMain"> <img id="main" src="' + item[0].imagePath + '" style="object-fit:contain;width:360px;height:360px;" alt="' + item[0].title + '" /> </div> </aside> <section> <div class="prodNamePriceTags"> <input type="text" class="form-control" style="float:right;width:100%" align="right" name="productName" id="productName" value="' + item[0].title + '"><br><br>' + '</h5> <br> <h6 class="col" style="float:right;width:100%;font-weight:normal">$' + item[0].price + '</h6> <br> <br> <div class="row" style="height:290px;width:100%;float:right"> <br> ' + item[0].description + ' </div><br> </div> </section> <br> <div class="col center2"> <center><strong>Tech Specs</strong></center><hr> <div class="col" style="width:100%;float:right">'; 
        tr += '</h1> <!--float:left;--><hr> <form id="form" autocomplete="off"> <aside> <div class="uploadImageMain"> <img id="main" src="' + item[0].imagePath + '" style="object-fit:contain;width:360px;height:360px;" alt="' + item[0].title + '" /> </div> </aside> <section> <div id="editNamePriceDesc"><input type="text" class="form-control" style="float:right;width:100%" align="right" name="productName" id="productName" value="' + item[0].title + '"><br><br><input type="number" min="0.01" step="0.01" onchange="setTwoNumberDecimal(this)" class="form-control" style="float:right;width:100%" align="right" name="price" id="price" value="' + item[0].price + '"><br><br>Description: <br><textarea id="desc" name="desc" class="form-control" style="height:240px;width:100%" >' + item[0].description + '</textarea><br> </div><br> </div> </section> <br> <div class="center2" id="editAdvOpt"><center><strong>Tech Specs</strong></center><hr><textarea id="specs" name="specs" class="form-control" style="height:240px;width:100%" >' + item[0].techspecs + '</textarea><br><center><strong>Advanced Options</strong></center><hr><br>Tags: <input type="text" id="tags" name="tags" class="form-control" style="float:right;width:95%" value="' + item[0].tags + '"><br><br></div>';
        //tr += '<form id="form" autocomplete="off"><aside><div class="image-upload"><label for="mainImage"><img id="main" src="images\placeholders\uploadImage.jpg" style="object-fit:contain;width:360px;height:360px;border-radius:5%;" alt="your image" /></label><input type="file" id="mainImage" style="display:none" onchange="readURL(this)"/></div></aside><section><div id="editNamePriceDesc"><input type="text" class="form-control" style="float:right;width:100%" align="right" name="productName" id="productName" placeholder="Product Name"><br><br><input type="number" min="0.01" step="0.01" onchange="setTwoNumberDecimal(this)" class="form-control" style="float:right;width:100%" align="right" name="price" id="price" placeholder="$0.00"><br><br>Description: <br><textarea id="desc" name="desc" class="form-control" style="height:240px;width:100%" ></textarea><br> <!--  <input type="text" name="tags" id="tags" style="height:235px;width:100%"> --></div>'
        //if(item[0].techspecs == null) tr += 'Here are some Specs. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
        //else tr += item[0].techspecs;
        tr += '</div><br> </div> <hr> <div class="center" id="saveCancelButtons"><!--<input type="submit" style="float:left" value="Save Changes">--><button type="button" id="Save" style="float:left" class="btn btn-primary" onclick="editData()">Save Changes</button><!--<input type="submit" style="float:right" value="Cancel">--><a href="../products/index.cfm"><button type="button" id="Cancel" style="float:right" class="btn btn-secondary">Cancel</button></a></div> <br><br><br><hr> </form> </div> </main>';
        $('#main').hide();
        $('#main').append(tr);
        $('#main').fadeIn();
    });
}

function copyDiv() {
    var productNameEntry = document.getElementById('productName');
    var productNamePreview = document.getElementById('namePreview');
    productNamePreview.innerHTML = productNameEntry.value;
    var priceEntry = document.getElementById('price');
    var pricePreview = document.getElementById('pricePreview');
    pricePreview.innerHTML = "$" + priceEntry.value;
    var descEntry = document.getElementById('desc');
    var descPreview = document.getElementById('descPreview');
    descPreview.innerHTML = descEntry.value;
    var specsEntry = document.getElementById('specs');
    var specsPreview = document.getElementById('specsPreview');
    specsPreview.innerHTML = specsEntry.value;
}
function showPreview() {
    copyDiv();
    var cpb = document.getElementById('preview');					//cpb = customer preview button
    cpb.id = 'exitPreview';
    cpb.onclick = showEdit;
    cpb.innerHTML = "Exit Preview";
    //var epb = document.getElementById('exitPreview');
    //epb.style.display = "block";
    var enpd = document.getElementById('editNamePriceDesc');		//enpd = edit name price desc
    enpd.style.display = "none";
    var pnd = document.getElementById('previewNameDesc');			//pnd = preview name desc
    pnd.style.display = "block";
    var eao = document.getElementById('editAdvOpt');				//eao = edit advanced option
    eao.style.display = "none";
    var pts = document.getElementById('previewTechSpec');			//pts = preview tech spec
    pts.style.display = "block";
    var scb = document.getElementById('saveCancelButtons');			//scb = save cancel buttons
    scb.style.display = "none";
    var cr = document.getElementById('custReviews');				//cr = customer reviews
    cr.style.display = "block";
}
function showEdit() {
    //var cpb = document.getElementById('preview');
    //cpb.style.display = "block";
    var epb = document.getElementById('exitPreview');
    epb.id = 'preview';
    epb.onclick = showPreview;
    epb.innerHTML = "Customer Preview";
    var enpd = document.getElementById('editNamePriceDesc');
    enpd.style.display = "block";
    var pnd = document.getElementById('previewNameDesc');
    pnd.style.display = "none";
    var eao = document.getElementById('editAdvOpt');
    eao.style.display = "block";
    var pts = document.getElementById('previewTechSpec');
    pts.style.display = "none";
    var scb = document.getElementById('saveCancelButtons');
    scb.style.display = "block";
    var cr = document.getElementById('custReviews');
    cr.style.display = "none";
}
function setTwoNumberDecimal(el) {
    el.value = parseFloat(el.value).toFixed(2);
}
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#main')
                .attr('src', e.target.result)
                .width(360)
                .height(360);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

$(document).ready(function () {
    $(Document).on("click", "#logout", function (e) { //logout of current user
      e.preventDefault();
      sessionStorage.removeItem('userID');
      sessionStorage.removeItem('role');
      sessionStorage.removeItem('sessionID');
      $('#logoutM').modal();
      //window.location.replace("../products/index.cfm");
    });
  
  });