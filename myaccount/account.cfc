<cfcomponent displayname="account" hint="CFC for My Account">
  <cfset this.datasource="AppStore">
  <cffunction name="login" access="remote" returnformat="plain">
    <cfargument name="uName" required="true" type="string">
    <cfargument name="pWord" required="true" type="string">
    <cfquery name="login" datasource = "#this.datasource#">
      SELECT Users.userID, Roles.role_name FROM Users LEFT JOIN Roles ON Users.userID = Roles.userID WHERE uName = <cfqueryparam value="#uName#" cfsqltype="cf_sql_varchar"> AND pWord = <cfqueryparam value="#pWord#" cfsqltype="cf_sql_varchar">;
    </cfquery>
    <cfreturn #serializeJSON(login, "struct")#/>
  </cffunction>
  <cffunction name="sales" access="remote" returnformat="plain">
    <cfquery name="sales" datasource="#this.datasource#">
      Select [Cart Items].[orderID], [Cart Items].[productID], [Products].title, [Orders].checkedOut FROM [Cart Items] INNER JOIN [Products] ON [Cart Items].productID = Products.productID INNER JOIN [Orders] ON [Cart Items].orderID = Orders.orderID;
    </cfquery>
    <cfreturn #serializeJSON(sales, "struct")#/>
  </cffunction>
  <cffunction name="salesTrends" access="remote" returnformat="plain">
    <cfargument name="begDate" required="true" type="string">
    <cfargument name="endDate" required="true" type="string">
    <cfquery name="salesTrends" datasource="#this.datasource#">
      Select [Cart Items].[orderID], [Cart Items].[productID], [Products].title, [Orders].checkedOut FROM [Cart Items] INNER JOIN [Products] ON [Cart Items].productID = Products.productID INNER JOIN [Orders] ON [Cart Items].orderID = Orders.orderID WHERE CAST([Orders].[checkedOut] AS DATE) >= CAST('#begDate#' AS DATE) AND CAST([Orders].[checkedOut] AS DATE) <= CAST('#endDate#' AS DATE);
    </cfquery>
    <cfreturn #serializeJSON(salesTrends, "struct")#/>
  </cffunction>
  <cffunction name="custOrders" access="remote"  returnformat="plain">
    <cfargument name="userID" required="true" type="numeric">
    <cfquery name="custOrders" datasource="#this.datasource#">
      SELECT orderID, checkedOut FROM Orders Where userID = <cfqueryparam value="#userID#" cfsqltype="cf_sql_integer"> AND checkedOut IS NOT NULL ORDER BY orderID OFFSET 0 ROWS FETCH NEXT 10 ROWS ONLY;
    </cfquery>
    <cfreturn #serializeJSON(custOrders, "struct")#/>
  </cffunction>
  <cffunction name="custOrdersAll" access="remote"  returnformat="plain">
    <cfquery name="custOrders" datasource="#this.datasource#">
      SELECT orderID, checkedOut FROM Orders WHERE checkedOut IS NOT NULL ORDER BY orderID DESC OFFSET 0 ROWS FETCH NEXT 10 ROWS ONLY;
    </cfquery>
    <cfreturn #serializeJSON(custOrders, "struct")#/>
  </cffunction>
  <cffunction name="priceTotal" access="remote"  returnformat="plain">
    <cfargument name="orderID" required="true" type="numeric">
    <cfquery name="custOrders" datasource="#this.datasource#">
      Select [Cart Items].[orderID], [Cart Items].[productID], [Products].price FROM [Cart Items] INNER JOIN [Products] ON [Cart Items].productID = Products.productID INNER JOIN [Orders] ON [Cart Items].orderID = Orders.orderID Where [Cart Items].orderID = <cfqueryparam value="#orderID#" cfsqltype="cf_sql_integer">;
    </cfquery>
    <cfreturn #serializeJSON(custOrders, "struct")#/>
  </cffunction>
</cfcomponent>
