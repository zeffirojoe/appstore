function populatePage() {
  $('#main').hide("slow", function () { //updated page based on login information
    $('#main').empty();
    $('#main').removeClass('d-none');
    if (sessionStorage.getItem('role') == 3) landingPage();
    else if (sessionStorage.getItem('role') == 2) userPage();

    $('#main').show('slow');
  });
}


function landingPage() {
  $('#main').append('<header class="masthead"> <div class="container h-100"> <div class="row h-100 align-items-center"> <div class="col-12 text-center"> <h1 class="font-weight-light">RDE AppStore Admin</h1> <p class="lead">Choose From Avaliable Options</p> </div> </div> </div> </header><!-- Page Content --> <section class="py-5"> <div class="container" style="padding-bottom: 25px !important"> <a class="btn btn-primary" href="#" id="salesData">Sales Trends</a> </div> <h3 class="font-weight-light">Recent Orders</h3> </section> <div id="custOrders"></div> ');
  $.get('account.cfc?method=custOrdersAll', function (data) {
    var items = JSON.parse(data);
    var tr = '<table class="table table-striped"><thead><tr><th scope="col">Order ID</th><th scope="col">Order Date</th><th scope="col">Total Cost</th></tr></thead><tbody>';
    var count = 1;
    var total = 0;
    for (var x = 0; x < items.length; x++) {
      if (items[x].checkedOut == null) continue;
      tr += '<tr>';
      tr += '<th scope="row">' + items[x].orderID + '</th>';
      tr += '<td>' + items[x].checkedOut.slice(0, -9) + '</td>';
      $.when(totalCost(items[x].orderID)).done(function (data) {
        var items = JSON.parse(data);
        var sum = 0;
        for (var y = 0; y < items.length; y++) sum += parseFloat(items[y].price);
        total = sum;
      });
      tr += '<td>$' + total + '</td>';
      tr += '</tr>';
      count++;
    }
    tr += '</tbody></table>';
    $('#custOrders').append(tr);
  });
}

function userPage() {
  $('#main').append('<header class="masthead"> <div class="container h-100"> <div class="row h-100 align-items-center"> <div class="col-12 text-center"> <h1 class="font-weight-light">RDE AppStore Customer</h1> <h3 class="font-weight-light">Recent Orders</h3> </div> </div> </div> </header><!-- Page Content --> <section class="py-5"> <div id="custOrders">  </div> </section> ');
  $.get('account.cfc?method=custOrders&userID=' + sessionStorage.getItem('userID'), function (data) {
    var items = JSON.parse(data);
    var tr = '<table class="table table-striped"><thead><tr><th scope="col">#</th><th scope="col">Order Date</th><th scope="col">Total Cost</th></tr></thead><tbody>';
    var count = 1;
    var total = 0;
    for (var x = 0; x < items.length; x++) {
      if (items[x].checkedOut == null) continue;
      tr += '<tr>';
      tr += '<th scope="row">' + count + '</th>';
      tr += '<td>' + items[x].checkedOut.slice(0, -9) + '</td>';
      $.when(totalCost(items[x].orderID)).done(function (data) {
        var items = JSON.parse(data);
        var sum = 0;
        for (var y = 0; y < items.length; y++) sum += parseFloat(items[y].price);
        total = sum;
      });
      tr += '<td>$' + total + '</td>';
      tr += '</tr>';
      count++;
    }
    tr += '</tbody></table>';
    $('#custOrders').append(tr);
  });
}

function totalCost(orderID) {
  return $.ajax({
    url: 'account.cfc?method=priceTotal&orderID=' + orderID,
    type: 'GET',
    async: false,
    cache: false,
    timeout: 30000,
    success: function (data) {
      var items = JSON.parse(data);
      var total = 0;
      for (var y = 0; y < items.length; y++) total += items[y].price;
      return total;
    }
  });

}

$(document).ready(function () {
	if(sessionStorage.getItem('userID') != null){
        var userID = sessionStorage.getItem('userID');
    	userID = parseFloat(userID);
    	$.get('../cart/cart.cfc?method=createSession&userID=' + userID);
	}
  $('#form').submit(function (e) { //grabs info from form, executed login and saves ID's into sessionStorage for use throughout the web application
    e.preventDefault();
    var username = $('#login').val();
    var pword = $('#password').val();
    $.get("account.cfc?method=login&uName=" + username + "&pWord=" + pword, function (data) {
      if (data == '[]') {
        $('#formFooter').text('Incorrect Login').css('color', 'red');
        return;
      }
      var txt = JSON.parse(data);
      sessionStorage.setItem('userID', txt[0].userID);
      sessionStorage.setItem('role', txt[0].role_name);
      sessionStorage.setItem('sessionID', '#' + txt[0].userID);
      if (sessionStorage.getItem('userID') == null) alert('Incorrect login');
      else {
        location.reload();
      }
    }).fail(function () {
      $('#formFooter').text('Incorrect Login').css('color', 'red');
    });
  });

  $(Document).on("click", "#logout", function (e) { //logout of current user
	$.get('../cart/cart.cfc?method=deleteSession');
    e.preventDefault();
    sessionStorage.removeItem('userID');
    sessionStorage.removeItem('role');
    sessionStorage.removeItem('sessionID');
    $('#logoutM').modal();

  });

  $(Document).on("click", "#salesData", function (e) {
    e.preventDefault();
    $('#main').empty();
    $('#main').removeClass('d-none');
    window.location.replace('../sales');
  });

});