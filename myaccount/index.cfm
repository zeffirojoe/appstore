<html>

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
  </script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
  </script>
  <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
  <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
  <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
  <script src="account.js"></script>
  <link rel="stylesheet" type="text/css" href="index.css">
  <link rel="icon" type="image/ico" href="../images/image_v3_MHH_icon.ico">
  <title>My Account</title>
</head>
<!---<cfoutput>#items#</cfoutput>--->

<body>
  <div class="modal fade" id="logoutM" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel4" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel4">Logout</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"  onclick="location.reload()">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Successfully Logged out of User
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="location.reload()">Close</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Insufficient Data</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Date Field Empty
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="pieModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel1">No Results</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          No Products were Purchased in that Date Range
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <div class="menu">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="container" id="navbar">
        <a class="navbar-brand" href="../home">
          <img src="https://i.gyazo.com/c29162661e819a80f550c3c2897db908.png" alt="Home">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
          aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="../home">Home
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../products">Products</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../cart/cart.cfm">Cart</a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="#">My Account
                <span class="sr-only">(current)</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </div>
  <script>
    if (sessionStorage.getItem('role') == 3) {
      $('a').filter(function (index) {
        return $(this).text() === "Cart";
      }).attr('href', '../sales');
      $('a').filter(function (index) {
        return $(this).text() === "Cart";
      }).text('Sales Trends');
    }
    if (sessionStorage.getItem('role') != null) {
      $('.navbar-nav').append('<li class="nav-item"><a class="nav-link" href="#" id="logout">Logout</a></li>');
    }
  </script>
  <div class="wrapper fadeInDown d-none" id="main">
    <div id="formContent">
      <!-- Tabs Titles -->

      <!-- Icon -->
      <div class="fadeIn first">
        <img src="../images/rde_logo.png" id="icon" alt="User Icon" />
      </div>

      <!-- Login Form -->
      <form id="form">
        <input type="text" id="login" class="fadeIn second" name="login" placeholder="login">
        <input type="password" id="password" class="fadeIn third" name="login" placeholder="password">
        <input type="submit" class="fadeIn fourth" value="Log In">
      </form>

      <!-- Remind Passowrd -->
      <div id="formFooter">
        <p class="underlineHover"></a>
      </div>
    </div>
  </div>
  <script>
    if (sessionStorage.getItem('userID') != null) {
      populatePage();
    } else $('#main').removeClass('d-none');
  </script>

  <!-- Footer -->
  <footer class="page-footer font-small blue" id="footer">

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">
      <p id="date">
        Date
      </p>
    </div>
    <!-- Copyright -->

  </footer>
  <!-- Footer -->
</body>
<script>
  var today = new Date();
  var date = today.getFullYear();
  var dateTime = date;
  $("#date").html(dateTime + '<a href="https://www.rde.org/"> RDE Systems LLC</a>');
</script>

</html>