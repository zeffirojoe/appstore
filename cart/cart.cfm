<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
      crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
      crossorigin="anonymous"></script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
      crossorigin="anonymous"></script>
    <script src="cartJS.js"></script>
    <link rel="stylesheet" type="text/css" href="cart.css">
	<link rel="stylesheet" type="text/css" href="/Octicons/package/build/build.css">
	<link rel="icon" type="image/ico" href="../images/image_v3_MHH_icon.ico">
    <title>Product List</title>
    <cfobject name="ct" component="cart">
	<script>
		var sess = sessionStorage.getItem('userID');
		if(sess == null){window.location.href = "../myaccount/index.cfm";}
		var foo = sessionStorage.getItem('orderID');
		if(foo == null){window.location.href = "../products/index.cfm";}
	</script>
    </head>
    <body>
	  <div class="modal fade" id="logoutM" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel40" aria-hidden="true">
	    <div class="modal-dialog" role="document">
	      <div class="modal-content">
	        <div class="modal-header">
	          <h5 class="modal-title" id="exampleModalLabel40">Logout</h5>
	          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	            <span aria-hidden="true">&times;</span>
	          </button>
	        </div>
	        <div class="modal-body">
	          Successfully Logged out of User
	        </div>
	        <div class="modal-footer">
	          <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="location.reload()">Close</button>
	        </div>
	      </div>
	    </div>
	  </div>

     <div class="menu">
     <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="container" id="navbar">
        <a class="navbar-brand" href="../home">
          <img src="https://i.gyazo.com/c29162661e819a80f550c3c2897db908.png" alt="Home">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
          aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="../home">Home
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../products">Products</a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="../cart/cart.cfm">Cart</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../myaccount">My Account</a>
                <span class="sr-only">(current)</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  	</div>
	<script>
      if(sessionStorage.getItem('role') != null){
        $('.navbar-nav').append('<li class="nav-item"><a class="nav-link" href="#" id="logout">Logout</a></li>');
      }
    </script>
  <!---> END OF THE NAV BAR</!--->
    <br><br><br><br>
    <div class="container-fluid" id="cartItems">
	<div class="row">

	<div class="col-md-9">
      <div class="row" id="header">
        <div class="col-md-5"><h2> My Cart</h2></div>
        <div class="col-md-5"></div>
        <div class="col-md-2"><h2>Price</h2></div>
    </div>
	<cfset userID= "#Session["userID"]#">
    <cfset currentOrder = "#ct.getOrder(userID=userID)#">
	<script>sessionStorage.setItem('currentOrder', <cfoutput>#currentOrder#</cfoutput>);</script>
    <cfset products = "#ct.getProducts(orderID=currentOrder)#">
    <cfset prices = ArrayNew(1)>

    <cfloop array="#products#" index="value" item="current">
      <cfset info ="#ct.productInfo(productID=current)#">
      <div id="<cfoutput>product#value#</cfoutput>" class="item" style="display: none;"><hr><div id="<cfoutput>price#value#</cfoutput>" class="row"> <div class="col-md-5"><a href="#"><img class="img-fluid rounded mb-5 mb-mb-0 cartItem" src=<cfoutput>"#info["imagePath"]#"</cfoutput>></a>
      </div> <div class="col-md-5 info"><h3 class="itemTitle"><cfoutput>#info["title"]#</cfoutput></h3><p><cfoutput>#info["description"]#</cfoutput></p>

      </div> <div class="col-md-2">  <br><br>
		<button id="<cfoutput>#current#</cfoutput>" type="button" class="btn btn-link delete">Delete <span><svg xmlns="http://www.w3.org/2000/svg" width="12" height="16" viewBox="0 0 12 16"><path fill-rule="evenodd" d="M11 2H9c0-.55-.45-1-1-1H5c-.55 0-1 .45-1 1H2c-.55 0-1 .45-1 1v1c0 .55.45 1 1 1v9c0 .55.45 1 1 1h7c.55 0 1-.45 1-1V5c.55 0 1-.45 1-1V3c0-.55-.45-1-1-1zm-1 12H3V5h1v8h1V5h1v8h1V5h1v8h1V5h1v9zm1-10H2V3h9v1z"/></svg></span></button>
		</div> </div></div>
    </cfloop>
	<hr>
	</div>

	<div class="col-md-3 columnDetails rounded">
		<cfset numProducts= arrayLen(products)>
		<cfset subtotal= 0>
		<h2 id="yourOrder" class="d-flex justify-content-between align-items-center mb-3">Your order<span class="badge badge-secondary badge-pill"><cfoutput>#numProducts#</cfoutput></span></h2>
		<ul class="list-group mb-3">
			<form id="checkoutForm" name="checkout" action="checkout.cfm" method="post">
			<input class="btn btn-primary" type="submit" id="checkoutSubmit" name="checkoutSubmit" value="Proceed To Checkout"><hr>
			<cfloop index="current" from="1" to="#numProducts#">
				<cfset info ="#ct.productInfo(productID=products[current])#">
				<cfset subtotal+= #info["price"]#>
				<input type="hidden" name="<cfoutput>product#current#</cfoutput>" value="<cfoutput>#info["title"]#</cfoutput>">
            	<input type="hidden" name="<cfoutput>price#current#</cfoutput>" value="<cfoutput>#info["price"]#</cfoutput>">
				<li class="itemDetail list-group-item d-flex justify-content-between lh-condensed">
									<div>
										<h6 class="my-0"><cfoutput>#info["title"]#</cfoutput></h6>
									</div>
									<span class="text-muted qty"><cfoutput>#info["price"]#</cfoutput></span>
				</li>
			</cfloop>
			<hr>
			<cfset taxes= NumberFormat(#subtotal# * 0.07, '9.99') >
			<cfset total= #subtotal# + #taxes# >
			<li id="subtotal" class="list-group-item d-flex justify-content-between">
								<span>Subtotal</span>
								<span class="text-muted"><span class="dollar">$</span><span id="subAmount"><cfoutput>#subtotal#</cfoutput></span></span>
						</li>
			<li id="taxes" class="list-group-item d-flex justify-content-between">
								<span>Taxes</span>
								<span class="text-muted"><span class="dollar">$</span><span id="taxAmount"><cfoutput>#taxes#</cfoutput></span></span>
						</li>
			<li id="total" class="list-group-item d-flex justify-content-between">
								<strong>Total</strong>
								<strong><span id="dollarTotal">$</span><span id="totalAmount"><cfoutput>#total#</cfoutput></span></strong>
						</li>
			</form>
		</ul>
	</div>
	</div>
    </div>
    <hr>

    <footer class="page-footer font-small blue" id="footer">
      <div class="footer-copyright text-center py-3">
        <p id="date">
          Date
          </p>
      </div>

    </footer>
  </body>
  <script>
    var today = new Date();
    var date = today.getFullYear();
    var dateTime = date;
    $("#date").html(dateTime + '<a href="https://www.rde.org/"> RDE Systems LLC</a>');
  </script>

</html>
