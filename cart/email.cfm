<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
			crossorigin="anonymous">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
			crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
			crossorigin="anonymous"></script>
		<script src="email.js"></script>
		<script type='text/javascript' src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
		<title>Product List</title>
		<cfobject name="ct" component="cart">
	</head>
	<body>
	<cfmail type="html" to="#FORM["EMAIL"]#" from="RDE" subject="Order Confirmation">
		<body>
		<div id="emailContent" style="width:50%;">
		<div>
		<h2>Hello <cfoutput>#FORM.FIRSTNAME#</cfoutput>,</h2>
		<p>Thank you for shopping with us. Please see a summary of your order details below.</p>
		</div>
		<hr>
		<h2 id="details">Details</h2>

		<cfloop collection="#FORM#" item="key">
							<cfif FindNoCase("product", #key#)>
								<cfset product = #key#>
								<cfset productNum = Right(#key#, 1)>
								<cfloop collection="#FORM#" item="innerKey">
									<cfif #innerKey# eq "PRICE#productNum#">
										<cfset price = #innerKey#>
									</cfif>
								</cfloop>


								<cfset currentProduct = "#FORM["#product#"]#">
								<cfset currentPrice = "#FORM["#price#"]#">
								<cfset subtotal+= #currentPrice#>
								<div style="width:200px;">
								<p class="my-0" style="font-size:13px;"><cfoutput>#currentProduct#</cfoutput>     <span class="text-muted" style="float:right;"><cfoutput>#currentPrice#</cfoutput></span></p>
								</div>
							</cfif>
		</cfloop>
		<hr>
								<div style="width:200px;">
								<span>Subtotal</span>
								<span class="text-muted" style="float:right;"><span class="dollar">$</span><cfoutput>#FORM["SUBTOTAL"]#</cfoutput></span>
								</div>
								<div style="width:200px;">
								<span>Taxes</span>
								<span class="text-muted" style="float:right;"><span class="dollar">$</span><cfoutput>#FORM["TAXES"]#</cfoutput></span>
								</div>
								<div style="width:200px;font-size:20px;">
								<strong>Total</strong>
								<strong style="float:right;"><span class="dollar">$</span><cfoutput>#FORM["TOTAL"]#</cfoutput></strong>
								</div>
		<hr>

		<div style="width:200px;">
		<span>Bill to:</span><br>
		<strong><cfoutput>#FORM["ADDRESS"]#</cfoutput>, <cfoutput>#FORM["TOWN/CITY"]#</cfoutput>, <cfoutput>#FORM["STATE"]#</cfoutput><br>
		<span><cfoutput>#FORM["POSTALCODE"]#</cfoutput></span></strong><br>
		</div>

		<hr>


		<p>We hope to see you again soon!</p>
		<a href="https://www.rde.org/"> RDE Systems LLC</a>


	</div>
	</body>
	</cfmail>

	<div class="modal fade" id="logoutM" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel40" aria-hidden="false">
	    <div class="modal-dialog" role="document">
	      <div class="modal-content">
	        <div class="modal-header">
	          <h5 class="modal-title" id="exampleModalLabel40">Order Placed</h5>
	          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	            <span aria-hidden="true">&times;</span>
	          </button>
	        </div>
	        <div class="modal-body">
	          Your order has been placed
	        </div>
	        <div class="modal-footer">
	          <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="location.href = '../home/';">Close</button>
	        </div>
	      </div>
	    </div>
	  </div>


	</body>
<html>