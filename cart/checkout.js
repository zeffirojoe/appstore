function saveOrder(order, user) {
	var order = order;
	var user = user;
	var firstName = $('#firstName').find('input')[0].value;
	var lastName = $('#lastName').find('input')[0].value;
	var street = $('#street')[0].value;
	var city = $('#town')[0].value;
	var postal = $('#postal')[0].value;
	var email = $('#email').find('input')[0].value;
	sessionStorage.removeItem('orderID');
	 $.get('cart.cfc?method=saveOrder&orderID=' + order + '&userID=' + user);
}

$(document).ready(function(){
	
	$("#phoneInput").inputmask({"mask": "(999) 999-9999"});
	$(Document).on("click", "#logout", function (e) { //logout of current user
	    e.preventDefault();
	    sessionStorage.removeItem('userID');
	    sessionStorage.removeItem('role');
	    sessionStorage.removeItem('sessionID');
	    $('#logoutM').modal();
	  });
});