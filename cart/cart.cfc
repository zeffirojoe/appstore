<cfcomponent displayname="Cart" hint="CFC for cart items">
<cfset this.datasource = "AppStore">
<cffunction name="getOrder" access="remote" returnformat="plain">
	<cfargument name = "userID" required="true" default="3" type="numeric">
	<cfquery name ="getCurrent" datasource="AppStore">
		SELECT [orderID] FROM Orders WHERE (userID = #arguments.userID# AND checkedOut IS NULL) ;
	</cfquery>
	<cfif getCurrent.RecordCount GT 0>
		<cfloop query="getCurrent">
			<cfset orderID=#orderID#>
		</cfloop>
	<cfelse>
		<cfreturn "no">
	</cfif>
	<cfreturn orderID/>
</cffunction>

<cffunction name="getProducts" access="remote" returnformat="plain">
	<cfargument name = "orderID" required="true" type="numeric">
	<cfquery name="productsInOrder" datasource="AppStore">
		SELECT [productID] FROM [Cart Items] WHERE orderID=#arguments.orderID# ;
	</cfquery>
	<cfset products=ArrayNew(1)>
	<cfloop query="productsInOrder">
		<cfset worked =ArrayAppend(products, #productID#, "true")>
	</cfloop>
	<cfreturn products>
</cffunction>

<cffunction name="productInfo" access="remote" returnformat="plain">
	<cfargument name="productID" required="true" type="numeric">
	<cfquery name="info" datasource="AppStore">
		SELECT [description], [imagePath], [price], [title] FROM Products WHERE productID=#arguments.productID#;
	</cfquery>
	<cfset infoStruct = StructNew()>
	<cfloop query="info">
		<cfset StructInsert(infoStruct, "description", #description#)>
		<cfset StructInsert(infoStruct, "imagePath", #imagePath#)>
		<cfset StructInsert(infoStruct, "price", #price#)>
		<cfset StructInsert(infoStruct, "title", #title#)>
	</cfloop>
	<cfreturn infoStruct>

</cffunction>

<cffunction name="delete" access="remote" returnformat="plain">
	<cfargument name="productID" required="true" type="numeric">
	<cfargument name="orderID" required="true" type="numeric">
	<cfquery name="deleteQ" datasource="AppStore">
		DELETE FROM [dbo].[Cart Items]
      	WHERE [productID] = #productID# AND [orderID] = #orderID#;
	</cfquery>
</cffunction>

<cffunction name="createSession" access="remote">
	<cfargument name="userID" required="true" type="numeric">
	<cfoutput>#userID#</cfoutput>
	<cfset Session.userID = #userID#>
</cffunction>

<cffunction name="deleteSession" access="remote" returnformat="plain">
	<cfset StructClear(Session)>
</cffunction>

<cffunction name="saveOrder" access="remote" returnformat="plain">
	<cfargument name = "orderID" required="true" type="numeric">
	<cfargument name = "userID" required="true" type="numeric">
	<cfset date = dateFormat(Now(),"yyyy-mm-dd") & " " & timeFormat(now(), "HH:mm:ss.SSS")>
	<cfquery name ="save" datasource="AppStore">
		UPDATE [dbo].[Orders]
  		SET [checkedOut] = '#date#'
 		WHERE (userID = #userID# AND orderID = #orderID#)
	</cfquery>

</cffunction>

</cfcomponent>
	<!---><cfquery name="getProducts" datasource="AppStore">
		SELECT [description], [imagePath], [price], [title] FROM Products WHERE ([productID] = 1 OR [productID] = 2 OR [productID] = 3);
	</cfquery><--->