function deleteItem(productID){
	var productID=productID;
	var currentOrder = sessionStorage.getItem('currentOrder');
	 $.get('cart.cfc?method=delete&productID=' + productID + '&orderID=' + currentOrder, function (data) {
	      console.log("working");
	 });
}

function getUserID(){
	var userID = sessionStorage.getItem('userID');
	$('#cartItems').append('<cfset userID = "' + userID + '">');
}

function createSession(){
	var userID = sessionStorage.getItem('userID');
	userID = parseFloat(userID);
	$.get('cart.cfc?method=createSession&userID=' + userID);
}

$(document).ready(function(){
	$('.item').each(function () { 
	    $(this).fadeIn();
	});
	

	
	  $(Document).on("click", "#logout", function (e) { //logout of current user
			$.get('cart.cfc?method=deleteSession');
		    e.preventDefault();
		    sessionStorage.removeItem('userID');
		    sessionStorage.removeItem('role');
		    sessionStorage.removeItem('sessionID');
		    $('#logoutM').modal();

		  });

	$(Document).on("click", ".delete", function () { //handler for all links for each item, grabs item title
		var item = $(this).closest(".item").find("h3").text();
		var productTBR, priceTBR;
		productTBR = $(this).closest(".item")[0].id;
		priceTBR = $(this).closest(".row")[0].id;
		var inputProduct = $('input[name=' + productTBR + ']');
		var inputPrice= $('input[name=' + priceTBR + ']');;
		$(inputProduct).remove();
		$(inputPrice).remove();
		var row = $(this).closest(".item");
		var productID = $(this).attr('id');
		var tbdeleted;
		var price;
		var form;
		/*$('input').each(function(){
			var current = $(this);
			debugger;
		});*/
		$('.itemDetail').each(function(){
			if($(this).find("h6").text() == item){
				tbdeleted = $(this);
				price = tbdeleted.find(".qty").text();
				price=parseFloat(price);
			}
		});
		var sub = $('#subAmount')[0].innerHTML;
		sub = (sub - price).toFixed(2);
		$('#subAmount')[0].innerHTML = sub;
		var tax = (sub * 0.07);
		tax = parseFloat(tax);
		sub = parseFloat(sub);
		var total = sub+tax;
		tax = parseFloat(Math.round(tax * 100) / 100).toFixed(2);
		sub = parseFloat(Math.round(sub * 100) / 100).toFixed(2);
		total = parseFloat(Math.round(total * 100) / 100).toFixed(2);
		$('#taxAmount')[0].innerHTML = tax;
		$('#totalAmount')[0].innerHTML = total;
		var badge = $('.badge')[0].innerHTML;
		badge -=1;
		$('.badge')[0].innerHTML = badge;
		tbdeleted.remove();
		$(row).remove();
		$("#"+productID).remove();
		deleteItem(productID);
	});
});



