<!---><cfdump var="#GetHttpRequestData()#" output="browser">
<cfdump var="#StructCount(FORM)#" output="browser">
<cfloop array="#FORM#" index="current">
	<cfset currentProduct = "<cfoutput>product#current#</cfoutput>">
	<cfset currentPrice = "<cfoutput>price#current#</cfoutput>">
	<cfoutput>#currentProduct #currentPrice##</cfoutput>
</cfloop><--->
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
			crossorigin="anonymous">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
			crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
			crossorigin="anonymous"></script>
		<script src="checkout.js"></script>
		<script type='text/javascript' src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
		<link rel="stylesheet" type="text/css" href="checkout.css">
		<title>Product List</title>
		<cfobject name="ct" component="cart">
			<script>
		var sess = sessionStorage.getItem('userID');
		if(sess == null){window.location.href = "../myaccount/index.cfm";}

	</script>
	</head>
	<body>
		<script>
			var order = sessionStorage.getItem('orderID');
			order=parseFloat(order);
			var user = sessionStorage.getItem('userID');
			user=parseFloat(user);
		</script>
		<div class="modal fade" id="logoutM" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel40" aria-hidden="true">
		    <div class="modal-dialog" role="document">
		      <div class="modal-content">
		        <div class="modal-header">
		          <h5 class="modal-title" id="exampleModalLabel40">Logout</h5>
		          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		            <span aria-hidden="true">&times;</span>
		          </button>
		        </div>
		        <div class="modal-body">
		          Successfully Logged out of User
		        </div>
		        <div class="modal-footer">
		          <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="location.reload()">Close</button>
		        </div>
		      </div>
		    </div>
		 </div>
		<div class="menu">
				<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
					<div class="container" id="navbar">
						<a class="navbar-brand" href="../home">
							<img src="https://i.gyazo.com/c29162661e819a80f550c3c2897db908.png" alt="Home">
						</a>
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
							aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-icon"></span>
						</button>
						<div class="collapse navbar-collapse" id="navbarResponsive">
							<ul class="navbar-nav ml-auto">
								<li class="nav-item">
									<a class="nav-link" href="../home">Home </a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="../products">Products
										<span class="sr-only">(current)</span>
									</a>
								</li>
								<li class="nav-item ">
									<a class="nav-link" href="../cart/cart.cfm">Cart</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="../myaccount">My Account</a>
								</li>
							</ul>
						</div>
					</div>
				</nav>
			</div>
			<script>
      			if(sessionStorage.getItem('role') != null){
       				 $('.navbar-nav').append('<li class="nav-item"><a class="nav-link" href="#" id="logout">Logout</a></li>');
      			}
    		</script>
			<br><br><br>
		<div class="container" id="checkoutCart">
			<form id="placeOrderForm" name="placeOrder" action="email.cfm" method="post">
			<div class="row">
				<div class="columnBilling col-md-8 rounded">
					<h2>Billing Details</h2><hr>
					<div class="row mr-0 ml-0 mb-3">
						<div class="col-md-6 pl-0" id="firstName">
							<h6>First name*</h6>
							<input id="firstName" class="form-control" type="text" name="firstName">
						</div>
						<div class="col-md-6 pr-0" id="lastName">
							<h6>Last name*</h6>
							<input id="lastName" class="form-control" type="text" name="lastName">
						</div>
					</div>
					<div class="mb-3">
					<h6 class="notName">Company name</h6>
					<input class="form-control" type="text" name="companyName">
					</div>
					<div class="mb-3">
					<h6 class="notName">Country*</h6>
					<select class="input-medium bfh-countries" data-country="US" disabled>
						<option value="US">United States</option>
					</select>
					</div>
					<div class="mb-3">
					<h6 class="notName">Address*</h6>
					<input id="street" class="form-control" type="text" name="address" placeholder="Street address"><br>
					<input class="form-control" type="text" name="street" placeholder="Apt, suite, etc. (optional)">
					</div>
					<div class="mb-3">
					<h6 class="notName">Town/City* </h6>
					<input id="town" class="form-control" type="text" name="town/city">
					</div>
					<div class="mb-3">
					<h6 class="notName">State/County* </h6>
					<select name="state" class="input-medium bfh-countries" form="placeOrderForm">
						 <option value="AL">Alabama</option>
						  <option value="AK">Alaska</option>
						  <option value="AZ">Arizona</option>
						  <option value="AR">Arkansas</option>
						  <option value="CA">California</option>
						  <option value="CO">Colorado</option>
						  <option value="CT">Connecticut</option>
						  <option value="DE">Delaware</option>
						  <option value="FL">Florida</option>
						  <option value="GA">Georgia</option>
						  <option value="HI">Hawaii</option>
						  <option value="ID">Idaho</option>
						  <option value="IL">Illinois</option>
						  <option value="IN">Indiana</option>
						  <option value="IA">Iowa</option>
						  <option value="KS">Kansas</option>
						  <option value="KY">Kentucky</option>
						  <option value="LA">Louisiana</option>
						  <option value="ME">Maine</option>
						  <option value="MD">Maryland</option>
						  <option value="MA">Massachusetts</option>
						  <option value="MI">Michigan</option>
						  <option value="MN">Minnesota</option>
						  <option value="MS">Mississippi</option>
						  <option value="MO">Missouri</option>
						  <option value="MT">Montana</option>
						  <option value="NE">Nebraska</option>
						  <option value="NV">Nevada</option>
						  <option value="NH">New Hampshire</option>
						  <option value="NJ">New Jersey</option>
						  <option value="NM">New Mexico</option>
						  <option value="NY">New York</option>
						  <option value="NC">North Carolina</option>
						  <option value="ND">North Dakota</option>
						  <option value="OH">Ohio</option>
						  <option value="OK">Oklahoma</option>
						  <option value="OR">Oregon</option>
						  <option value="PA">Pennsylvania</option>
						  <option value="RI">Rhode Island</option>
						  <option value="SC">South Carolina</option>
						  <option value="SD">South Dakota</option>
						  <option value="TN">Tennessee</option>
						  <option value="TX">Texas</option>
						  <option value="UT">Utah</option>
						  <option value="VT">Vermont</option>
						  <option value="VA">Virginia</option>
						  <option value="WA">Washington</option>
						  <option value="WV">West Virginia</option>
						  <option value="WI">Wisconsin</option>
						  <option value="WY">Wyoming</option>
					</select>
					</div>
					<div class="mb-3">
					<h6 class="notName">Postal/Zip code* </h6>
					<input id="postal" class="form-control" type="text" name="postalCode">
					</div>
					<div class="contactInfo row mr-0 ml-0">
						<div class="col-md-6 notName pl-0" id="phone">
							<h6>Phone*</h6>
							<input id="phoneInput" class="form-control" type="text" name="phone">
						</div>
						<div class="col-md-6 notName pr-0" id="email">
							<h6>Email*</h6>
							<input id="email" type="email" class="form-control" type="text" name="email" minlength="8" maxlength="100">
						</div>
					</div>
				</div>

				<div class="columnDetails col-md-4 rounded">
					<cfset numProducts= ((StructCount(FORM)-2)/2)>
					<cfset subtotal= 0>
					<h2 class="d-flex justify-content-between align-items-center mb-3">Your order<span class="badge badge-secondary badge-pill"><cfoutput>#numProducts#</cfoutput></span></h2>
					<hr id="yourOrder">
					<ul class="list-group mb-3">
						<cfloop collection="#FORM#" item="key">
							<cfif FindNoCase("product", #key#)>
								<cfset product = #key#>
								<cfset productNum = Right(#key#, 1)>
								<cfloop collection="#FORM#" item="innerKey">
									<cfif #innerKey# eq "PRICE#productNum#">
										<cfset price = #innerKey#>
									</cfif>
								</cfloop>


							<cfset currentProduct = "#FORM["#product#"]#">
							<cfset currentPrice = "#FORM["#price#"]#">
							<cfset subtotal+= #currentPrice#>
							<li class="list-group-item d-flex justify-content-between lh-condensed">
												<div>
													<h6 class="my-0"><cfoutput>#currentProduct#</cfoutput></h6>
												</div>
												<span class="text-muted"><cfoutput>#currentPrice#</cfoutput></span>
							</li>
							<input type="hidden" name="<cfoutput>#product#</cfoutput>" value="<cfoutput>#currentProduct#</cfoutput>">
            				<input type="hidden" name="<cfoutput>#price#</cfoutput>" value="<cfoutput>#currentPrice#</cfoutput>">
							</cfif>
						</cfloop>
						<hr>
						<cfset taxes= NumberFormat(#subtotal# * 0.07, '9.99') >
						<cfset total= #subtotal# + #taxes# >
						<input type="hidden" name="subtotal" value="<cfoutput>#subtotal#</cfoutput>">
						<input type="hidden" name="taxes" value="<cfoutput>#taxes#</cfoutput>">
						<input type="hidden" name="total" value="<cfoutput>#total#</cfoutput>">
						<li class="list-group-item d-flex justify-content-between">
											<span>Subtotal</span>
											<span class="text-muted"><span class="dollar">$</span><cfoutput>#subtotal#</cfoutput></span>
									</li>
						<li class="list-group-item d-flex justify-content-between">
											<span>Taxes</span>
											<span class="text-muted"><span class="dollar">$</span><cfoutput>#taxes#</cfoutput></span>
									</li>
						<li id="total" class="list-group-item d-flex justify-content-between">
											<strong>Total</strong>
											<strong><span class="dollar">$</span><cfoutput>#total#</cfoutput></strong>
						</li>
					</ul>
				</div>


			</div><br>
			<input class="form-control" onclick="saveOrder(order,user)" type="submit" id="checkout" name="checkOut" value="Place Order">
			</form>
		</div><br><br><br>

	<footer class="page-footer font-small blue" id="footer">
      <div class="footer-copyright text-center py-3">
        <p id="date">
          Date
          </p>
      </div>
    </footer>

  </body>
  <script>
    var today = new Date();
    var date = today.getFullYear();
    var dateTime = date;
    $("#date").html(dateTime + '<a href="https://www.rde.org/"> RDE Systems LLC</a>');
  </script>
	</html>