function populatePage() {
  $('#main').hide("slow", function () { //updated page based on login information
    $('#main').empty();
    $('#main').removeClass('d-none');
    if (sessionStorage.getItem('role') == 3) salesTrends();
    $('#main').show('slow');
  });
}

function random_rgba() {
  var colours = ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360", "#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360", "#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360", "#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"];
  var min = Math.ceil(19);
  var max = Math.floor(0);
  var o = Math.floor(Math.random() * (max - min + 1)) + min;
  return colours[o];
}

function lineChart(info, keys) {
  var split1 = $('#datepicker').val().split('/');
  var split2 = $('#datepicker2').val().split('/');
  var begDate = split1[2] + '-' + split1[0] + '-' + split1[1];
  var endDate = split2[2] + '-' + split2[0] + '-' + split2[1];
  var dates = [new Date(begDate)];
  var sets = [];
  for (var key in info) {
    var count = [];
    var date = [];
    var index;
    for (var x = 0; x < info[key].length; x++) {
      index = date.indexOf(info[key][x]);
      if (index == -1) {
        date.push(info[key][x]);
        count.push(1);
      } else {
        count[index] = count[index] + 1;
      }
    }
    var sets2 = [];
    for (var y = 0; y < date.length; y++) {
      var months = {
        'January,': '01',
        'February,': '02',
        'March,': '03',
        'April,': '04',
        'May,': '05',
        'June,': '06',
        'July,': '07',
        'August,': '08',
        'September,': '09',
        'October,': '10',
        'November,': '11',
        'December,': '12'
      };
      var dateTest = date[y].split(' ');
      var specificDate = dateTest[2] + '-' + months[dateTest[0]] + '-' + dateTest[1];
      specificDate = new Date(specificDate);
      console.log(specificDate);
      for (var u = 0; u < dates.length; u++) {
        if (dates[u].getTime() == specificDate.getTime()) break;
        if (u == (dates.length - 1)) {
          dates.push(specificDate);
        }
      }
      var set = {
        t: new Date(specificDate),
        y: count[y]
      };
      sets2.push(set);
    }
    var color = random_rgba();
    var dataset = {
      label: key,
      fill: false,
      data: sets2,
      borderWidth: 3,
      backgroundColor: [
        color
      ],
      borderColor: [
        color
      ],
    };
    sets.push(dataset);
  }
  //console.log(sets);
  dates.push(new Date(endDate));
  console.log(dates);
  for (var h = 0; h < sets.length; h++) {
    var length = sets[h].data.length;
    var pushSet = sets[h].data.slice(0);
    for (var g = 0; g < length; g++) {
      //console.log(sets[h].data[g].t);
      var tester = 0;
      for (var d = 0; d < dates.length; d++) {
        if (pushSet.length == dates.length) break;
        /*console.log(sets[h].data[g].t.getTime());
        console.log(sets[h].data[g].t);
        console.log(dates[d].getTime());
        console.log(dates[d]);*/
        if (dates[d].getTime() == sets[h].data[g].t.getTime()) {
          tester = 1;
          continue;
        } else if (tester == 0) {
          var setTest = {
            t: dates[d],
            y: 0
          };
          pushSet.unshift(setTest);
        } else if (tester == 1) {
          var setTest2 = {
            t: dates[d],
            y: 0
          };
          pushSet.push(setTest2);
        }
      }
      sets[h].data = pushSet;
    }
  }
  console.log(sets);
  for (var q = 0; q < dates.length; q++) {
    dates[q] = dates[q].toLocaleDateString();
  }
  var ctx = document.getElementById("lineChart").getContext("2d");
  new Chart(ctx, {
    type: 'line',
    lineThickness: 5,
    data: {
      labels: dates,
      datasets: sets

    },
    options: {
      scales: {
        xAxes: [{
          /*type: 'time',
          time: {
            displayFormats: {
              quarter: 'MMM YYYY'            
            }
          },*/
          ticks: {
            autoSkip: true,
            maxTicksLimit: 10
          }
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      }
    }
  });
}

function pieChart(x, y) {
  var ctxP = document.getElementById("pieChart").getContext('2d');
  new Chart(ctxP, {
    type: 'pie',
    data: {
      labels: x,
      datasets: [{
        data: y,
        backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360", "#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"],
        hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774", "#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"]
      }]
    },
    options: {
      responsive: true
    }
  });
  var tr = '<div id="pieTable" style="float: right"><table class="table table-striped"><thead><tr><th scope="col">Product Name</th><th scope="col">Amount Sold</th></tr></thead><tbody>';
  for (var i = 0; i < x.length; i++) {
    tr += '<tr>';
    tr += '<th scope="row">' + x[i] + '</th>';
    tr += '<td>' + y[i] + '</td>';
    tr += '</tr>';
  }
  tr += '</tbody></table></div>';
  $('.table').remove();
  $('#pieGraph').append(tr);
}


function totalCost(orderID) {
  return $.ajax({
    url: 'account.cfc?method=priceTotal&orderID=' + orderID,
    type: 'GET',
    async: false,
    cache: false,
    timeout: 30000,
    success: function (data) {
      var items = JSON.parse(data);
      var total = 0;
      for (var y = 0; y < items.length; y++) total += items[y].PRICE;
      return total;
    }
  });

}

function salesTrends() {
  $.get('sales.cfc?method=sales', function (data) { //gets items in carts or previously purchased
    var items = JSON.parse(data);
    var today = new Date();
    $('#main').append('<div id="toggleBtns"> <div class="btn-group" role="group" aria-label="Basic example"> <button type="button" class="btn btn-secondary" id="pieBtn">Pie Chart</button> <button type="button" class="btn btn-secondary" id="barBtn">Bar Graph</button> <button type="button" class="btn btn-secondary" id="lineBtn">Line Plot</button> </div> </div><br><br> ');
    $('#main').append('<div id="salesTrends"><hr><div class="dates row"><div><label for="datepicker">Beginning Date:</label><input class="date" id="datepicker" readOnly width="276" value="01/01/' + String(today.getFullYear()) + '" /><script> $("#datepicker").datepicker({ uiLibrary: "bootstrap4" }); </script> </div> <div class="divider"></div> <div> <label for="datepicker2">Ending Date:</label><input class="date" readOnly id="datepicker2" width="276" value="' + String((today.getMonth() + 1)) + '/' + String(today.getDate()) + '/' + String(today.getFullYear()) + '"/><script> $("#datepicker2").datepicker({ uiLibrary: "bootstrap4" }); </script></div></div> <br> <a class="btn btn-primary" href="#" id="pieG">Reload Data</a></div><br><div id="pieGraph"><div id="pieGraph1" style="float: left"><canvas id="pieChart"></canvas></div></div><div id="barGraph"> <hr><h3>All Items Sold</h3> <canvas id="horizontalBar"></canvas></div><div id="lineGraph"><canvas id="lineChart"></canvas></div>');
    $('#barGraph').hide();
    $('#lineGraph').hide();
    var count = [];
    var title = [];
    var index;
    for (var x = 0; x < items.length; x++) {
      if (items[x].checkedOut == null) continue;
      index = title.indexOf(items[x].title);
      if (index == -1) {
        title.push(items[x].title);
        count.push(1);
      } else {
        count[index] = count[index] + 1;
      }
    }
    pieChart(title, count);
    $('#pieG').click();
    new Chart(document.getElementById("horizontalBar"), {
      "type": "horizontalBar",
      "data": {
        "labels": title,
        "datasets": [{
          "label": "Total Sales",
          "data": count,
          "fill": false,
          "backgroundColor": ["rgba(255, 99, 132, 0.2)", "rgba(255, 159, 64, 0.2)",
            "rgba(255, 205, 86, 0.2)", "rgba(75, 192, 192, 0.2)", "rgba(54, 162, 235, 0.2)",
            "rgba(153, 102, 255, 0.2)", "rgba(201, 203, 207, 0.2)"
          ],
          "borderColor": ["rgb(255, 99, 132)", "rgb(255, 159, 64)", "rgb(255, 205, 86)",
            "rgb(75, 192, 192)", "rgb(54, 162, 235)", "rgb(153, 102, 255)", "rgb(201, 203, 207)"
          ],
          "borderWidth": 1
        }]
      },
      "options": {
        "scales": {
          "xAxes": [{
            "ticks": {
              "min": 0,
              "beginAtZero": true,
              callback: function (value, index, values) {
                if (Math.floor(value) === value) {
                  return value;
                }
              }

            }
          }]
        }
      }
    });
  });
}
$(document).ready(function () {

  $(Document).on("click", "#logout", function (e) { //logout of current user
    e.preventDefault();
    sessionStorage.removeItem('userID');
    sessionStorage.removeItem('role');
    sessionStorage.removeItem('sessionID');
    $('#logoutM').modal();
  });

  $(Document).on("click", "#salesData", function (e) {
    e.preventDefault();
    $('#main').empty();
    $('#main').removeClass('d-none');
    salesTrends();
  });

  $(Document).on("click", "#back", function (e) {
    e.preventDefault();
    $('#main').empty();
    $('#main').removeClass('d-none');
    populatePage();
  });


  $(Document).on("click", "#barBtn", function (e) {
    e.preventDefault();
    $('#pieGraph').hide();
    $('#salesTrends').hide();
    $('#lineGraph').hide();
    $('#barGraph').removeClass('d-none');
    $('#barGraph').fadeIn();
  });


  $(Document).on("click", "#pieBtn", function (e) {
    e.preventDefault();
    $('#barGraph').hide();
    $('#lineGraph').hide();
    $('#salesTrends').removeClass('d-none');
    $('#salesTrends').fadeIn();
    $('#pieGraph').removeClass('d-none');
    $('#pieGraph').fadeIn();
  });

  $(Document).on("click", "#lineBtn", function (e) {
    e.preventDefault();
    $('#barGraph').hide();
    $('#pieGraph').hide();
    $('#salesTrends').removeClass('d-none');
    $('#salesTrends').fadeIn();
    $('#lineGraph').removeClass('d-none');
    $('#lineGraph').fadeIn();
  });


  $(Document).on('click', '#pieG', function (e) {
    e.preventDefault();
    if ($('#datepicker').val() == '' || $('#datepicker2').val() == '') {
      $('#myModal').modal();
      return;
    }
    var split1 = $('#datepicker').val().split('/');
    var split2 = $('#datepicker2').val().split('/');
    var begDate = split1[2] + '-' + split1[0] + '-' + split1[1];
    var endDate = split2[2] + '-' + split2[0] + '-' + split2[1];
    $.get('sales.cfc?method=salesTrends&begDate=' + begDate + '&endDate=' + endDate, function (data) { //gets items in carts or previously purchased
      if (data == '[]') {
        $('#pieModal').modal();
        return;
      }
      var items = JSON.parse(data);
      var count = [];
      var title = [];
      var index;
      for (var x = 0; x < items.length; x++) {
        if (items[x].checkedOut == null) continue;
        index = title.indexOf(items[x].title);
        if (index == -1) {
          title.push(items[x].title);
          count.push(1);
        } else {
          count[index] = count[index] + 1;
        }
      }
      var info = {};
      for (var z = 0; z < items.length; z++) {
        if (items[z].checkedOut == null) continue;
        if (info[items[z].title] == null) info[items[z].title] = [items[z].checkedOut.slice(0, -9)];
        else info[items[z].title].push(items[z].checkedOut.slice(0, -9));
      }
      lineChart(info, title);
      pieChart(title, count);
    });
  });
});